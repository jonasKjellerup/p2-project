import * as protocol from '../common/relay-protocol.js'
import assert from 'assert'
import {Duplex} from 'stream';
import {randomBytes} from 'crypto';
import curve25519 from '@p2/curve25519-native'


class MockConnection extends Duplex {
    constructor(incoming, outgoing, options) {
        super(options);
        this.readSource = incoming || [];
        this.writeTarget = outgoing;
    }

    _read(size) {
        this.push(this.readSource.shift());
    }

    _write(chunk, encoding, callback) {
        this.writeTarget.push(chunk);
        callback();
    }

    static createPair() {
        const aBuffer = [];
        const bBuffer = [];
        return [
            new MockConnection(aBuffer,bBuffer),
            new MockConnection(bBuffer,aBuffer)
        ];
    }
}

function assert_buffer_equal(a, b) {
    assert.strictEqual(Buffer.compare(a, b), 0, "given buffers not equal");

}

describe('protocol.PacketParser', () => {

    describe('when given exact well-formed packet and internal buffers are empty', () => {
        const socket = new protocol.PacketParser();
        const packetBuffer = Buffer.from([10, 0, 5, 4, 9, 3, 4, 2]);
        socket.write(packetBuffer);
        let packet = socket.nextPacket;
        it('produces correct packet', async () => {
            if (packet instanceof Promise) {
                packet = await packet;
            }

            assert.strictEqual(packet.id, 10);
            assert.strictEqual(packet.payloadSize, 5);
            assert_buffer_equal(packetBuffer.slice(protocol.headerSize), packet.payload);
        })
        it('internal buffer is null and work packet empty', async () => {
            if (packet instanceof Promise) {
                packet = await packet;
            }
            packet = socket.workPacket;

            assert.strictEqual(packet.id, 0);
            assert.strictEqual(packet.payloadSize, -1);
            assert.strictEqual(packet.payload, null);
            assert.strictEqual(socket.workBuffer, null);
        })
    })

    describe('when given non-exact well-formed packet and internal buffers are empty', () => {
        const parser = new protocol.PacketParser();
        const packetBuffer = Buffer.from([10, 0, 5, 4, 9, 3, 4, 2, 12, 24, 56]);
        parser.write(packetBuffer);
        let packet = parser.nextPacket;
        it('produces correct packet', async () => {
            if (packet instanceof Promise) {
                packet = await packet;
            }

            assert.strictEqual(packet.id, 10);
            assert.strictEqual(packet.payloadSize, 5);
            assert_buffer_equal(packetBuffer.slice(protocol.headerSize, protocol.headerSize + packet.payloadSize), packet.payload);
        })
        it('internal work packet is partially filled', async () => {
            if (packet instanceof Promise) {
                packet = await packet;
            }
            packet = parser.workPacket;

            assert.strictEqual(packet.id, 12);
            assert.strictEqual(packet.payloadSize, (24 << 8) + 56);
            assert.strictEqual(packet.payload, null);
            assert.strictEqual(parser.workBuffer, null);
        })
    })

    describe('when given partial packet and internal buffers are non empty', () => {
        const parser = new protocol.PacketParser();
        const packetBuffer = Buffer.from([3, 4, 2]);
        const leftovers = Buffer.from([10, 0, 5, 4, 9]);
        parser.write(leftovers);
        parser.write(packetBuffer);
        let packet = parser.nextPacket;
        it('produces correct packet', async () => {
            if (packet instanceof Promise) {
                packet = await packet;
            }

            assert.strictEqual(packet.id, 10);
            assert.strictEqual(packet.payloadSize, 5);
            assert_buffer_equal(Buffer.concat([leftovers, packetBuffer]).slice(protocol.headerSize), packet.payload);
        })
        it('internal buffers are empty', async () => {
            if (packet instanceof Promise) {
                packet = await packet;
            }
            packet = parser.workPacket;

            assert.strictEqual(packet.id, 0);
            assert.strictEqual(packet.payloadSize, -1);
            assert.strictEqual(packet.payload, null);
            assert.strictEqual(parser.workBuffer, null);
        })
    })

    describe('when given exact zero-sized packet and internal buffers are empty', () => {
        const parser = new protocol.PacketParser();
        const packetBuffer = Buffer.from([0xA4, 0, 0]);
        parser.write(packetBuffer);
        let packet = parser.nextPacket;

        it('produces correct packet where payload=null', async () => {
            if (packet instanceof Promise) {
                packet = await packet;
            }

            assert.strictEqual(packet.id, 0xA4);
            assert.strictEqual(packet.payloadSize, 0);
            assert.strictEqual(packet.payload, null);
        });

        it('internal buffers are empty', async () => {
            if (packet instanceof Promise) {
                packet = await packet;
            }
            packet = parser.workPacket;

            assert.strictEqual(packet.id, 0);
            assert.strictEqual(packet.payloadSize, -1);
            assert.strictEqual(packet.payload, null);
            assert.strictEqual(parser.workBuffer, null);
        })
    });

    describe('when given non-exact zero-sized packet and internal buffers are empty', () => {
        const parser = new protocol.PacketParser();
        const packetBuffer = Buffer.from([0xA5, 0, 0, 1, 2, 3, 4]);
        parser.write(packetBuffer);
        let packet = parser.nextPacket;

        it('produces correct packet where payload=null', async () => {
            if (packet instanceof Promise) {
                packet = await packet;
            }

            assert.strictEqual(packet.id, 0xA5);
            assert.strictEqual(packet.payloadSize, 0);
            assert.strictEqual(packet.payload, null);
        });

        it('internal buffers/workPacket are non-empty', async () => {
            if (packet instanceof Promise) {
                packet = await packet;
            }
            packet = parser.workPacket;

            assert.strictEqual(packet.id, 1);
            assert.strictEqual(packet.payloadSize, (2 << 8) + 3);
            assert.strictEqual(packet.payload, null);
            assert_buffer_equal(parser.workBuffer, Buffer.from([4]))
        })
    });


})

describe('protocol.PacketParser#[@@iterator]', function () {
    const packets = [
        protocol.create_response(protocol.responseOk, "response was ok"),
        protocol.create_response(protocol.responseFailure, "response was a failure"),
    ];
    const socket = new protocol.PacketParser();
    socket.write(Buffer.concat(packets))

    it('yields proper packets', async () => {
        let i = 0;
        for await (const packet of socket) {

            assert.strictEqual(packet.id, protocol.packetResponse);
            assert.strictEqual(packet.payloadSize, packets[i].length - protocol.headerSize);
            assert_buffer_equal(packet.payload, packets[i].slice(protocol.headerSize));

            if (++i === packets.length) break;
        }
    })
});

function createKeys() {
    return curve25519.createKeyPair(randomBytes(32));
}

describe('RelayClient', function () {
    const aKeys = createKeys();
    const bKeys = createKeys();

    describe('#start_session', function () {
        // Test handling of valid key
        // Test handling of invalid key
        // Test handling of error cases
    });

    describe('#create_session', function () {
        // Test without remotes
        // Test with remotes
        // Test handling of error cases
    });
});
