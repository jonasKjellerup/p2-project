import libsignal from '@p2/libsignal'
import * as assert from "assert";

const crypto = libsignal.crypto;

describe("SessionBuilder", function () {
    const hubAddress = new libsignal.SignalProtocolAddress("localhost", 1);
    const remoteAddress = new libsignal.SignalProtocolAddress("localhost", 2);

    describe("prekey exchange", function () {
        const hubStore = new libsignal.SignalStore(":memory:");
        const remoteStore = new libsignal.SignalStore(":memory:");

        hubStore.identity = crypto.createKeyPair();
        remoteStore.identity = crypto.createKeyPair();

        const preKey = crypto.createKeyPair();
        // The key id does not affect the result in any meaningful
        // way and has been selected arbitrarily.
        const signedPreKey = libsignal.KeyHelper.generateSignedPreKey(remoteStore.identity, 1);

        const hubSessionBuilder = new libsignal.SessionBuilder(hubStore, remoteAddress);
        const hubSession = hubSessionBuilder.create_session({
            identityKey: remoteStore.identity.pubKey,
            preKey: {
                keyId: 2,
                publicKey: preKey.pubKey,
            },
            signedPreKey
        });

        const remoteSessionBuilder = new libsignal.SessionBuilder(remoteStore, hubAddress);
        let remoteSession;

        const plainTextMessage = Buffer.from("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua");
        const cipherTexts = [];
        const hubSessionCipher = new libsignal.SessionCipher(hubStore, hubSession);
        let remoteSessionCipher;

        it("the local hub can encrypt via prekey and the remote can decrypt", async function () {
            const cipherText = await hubSessionCipher.encrypt(plainTextMessage);
            cipherTexts.push(cipherText.body);

            remoteSession = remoteSessionBuilder.create_session_from_prekeys({
                identityKey: hubStore.identity.pubKey,
                preKey,
                signedPreKey: signedPreKey.keyPair,
                baseKey: hubSession.pendingPreKey.baseKey
            });

            remoteSessionCipher = new libsignal.SessionCipher(remoteStore, remoteSession);


            const plainText = await remoteSessionCipher.decryptWhisperMessage(cipherText.body);
            assert.strictEqual(
                Buffer.compare(plainTextMessage, plainText),
                0
            );
        });

        it("encryption continues to function after the prekey has been used", async function () {
            const cipherText = await hubSessionCipher.encrypt(plainTextMessage);
            cipherTexts.push(cipherText.body);
            const plainText = await remoteSessionCipher.decryptWhisperMessage(cipherText.body);

            assert.strictEqual(
                Buffer.compare(plainTextMessage, plainText),
                0
            );
        });

        // All of the above tests have the hubSession doing the encryption
        // and the remote doing the decryption. This test, tests the opposite.
        it("communication from remote to hub is functional", async function () {
            const cipherText = await remoteSessionCipher.encrypt(plainTextMessage);
            cipherTexts.push(cipherText.body);
            const plainText = await hubSessionCipher.decryptWhisperMessage(cipherText.body);

            assert.strictEqual(
                Buffer.compare(plainTextMessage, plainText),
                0
            );
        });

        // This ensures checks that the forward secrecy is upheld.
        it("Produces non-identical ciphertexts from identical plaintexts", function () {
            assert.notStrictEqual(
                Buffer.compare(cipherTexts[0], cipherTexts[1]),
                0
            );
            assert.notStrictEqual(
                Buffer.compare(cipherTexts[1], cipherTexts[2]),
                0
            );
        });
    });

});


