#!/usr/bin/env bash

mkdir ./keys

echo $(uname -s)

if [ -z "$1" ]
  then
    echo "Expected at least one argument (key-name)."
    exit
fi


case $(uname -s) in
    MINGW*) subjStr="//C=US\ST=Oregon\L=Portland\O=Company Name\OU=Org\CN=localhost";;
    *)      subjStr="/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=localhost"
esac

# Creates self-signed certificate and key for local development use.
openssl req -x509 -new -newkey rsa:4096 -sha256 -days 3650 -nodes \
  -keyout "./keys/$1-key.pem" -out "./keys/$1-cert.pem" \
  -subj "$subjStr"
