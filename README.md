# Install

Before running the program, all dependencies must first be installed. This can be done by running the following commands
in the terminal:
```bash
npm install

cd remote-client
npm install

cd ../local-hub
npm install
```

# Starting the remote client

The remote client can be started using the following commands:
```bash
cd remote-client
npm run start
```

If the relay is running using a self-signed certificate, the environmental variable `USE_SELF_SIGNED_CA` must be set.

By default, the remote client uses the following ports, these ports can be changed by setting the given environmental variables:

 - Authentication server port: 3000 (variable name: `RD_REMOTE_AUTH_PORT`)
 - Remote scan port: 4000 (variable name: `RD_REMOTE_SCAN_PORT`)
 - Local scan port: 4000 (variable name: `RD_LOCAL_SCAN_PORT`)

In case both the remote client and local hub are running on the same system either the remote or local scan ports should be changed
such that they are non-identical.

# Starting the local hub

The local hub can be started using the following commands:
```bash
cd local-hub
npm run start
```

If the relay is running using a self-signed certificate the environmental variable `RELAY_CA`
must be set to the path of the certificate file (example: `RELAY_CA=./keys/relay-cert.pem`);

# Starting the relay

The relay can be started using the following command:
```bash
npm run start-relay
```

The relay is configured using the `config.mjs` file. An example of this file shown below:
```js
export default{
    "relayPort": 62000,
    "relayLocalAddress": "0.0.0.0",
    "relayKey": "./keys/relay-key.pem",
    "relayCert": "./keys/relay-cert.pem",
    "relayDbUrl": "mongodb://hextech.dk:27017/"
}
```

To run the relay you must have a MongoDB instance available.

# Generating keys
A shell script for generating keys and self-signed certificates has been included for convenience’s sake.
The script is called `keygen.sh` and is found in the root directory of this repository.

The script is used as following:
```bash
./keygen.sh <key-name>
```

This will generate the output files `<key-name>-key.pem` and `<key-name>-cert.pem` in the `./keys` directory. 

When using the `USE_SELF_SIGNED_CA` variable on the remote client it will expect to be able to find the `relay-cert.pem`
in the `keys` folder located at the root of the project directory. This must match the certificate used by the relay.

Likewise, the local hub will expect to find `auth-serv-key.pem` and `auth-serv-cert.pem` in the aforementioned folder.
