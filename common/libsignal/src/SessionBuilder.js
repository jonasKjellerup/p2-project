const SignalProtocolAddress = require('./SignalProtocolAddress');

function SessionBuilder(storage, remoteAddress) {
    this.remoteAddress = remoteAddress;
    this.storage = storage;
}

const Internal = {
    crypto: require('./crypto.js').cryptoInternal,
    SessionLock: require('./SessionLock.js'),
    Record: require('./SessionRecord.js'),
};

class SignalSession {

    constructor(
        masterKey, lastRemoteEphemeralKey, remoteIdentity,
        address, storageId = -1
    ) {
        this.oldRatchetList = [];

        /**
         * @type {Map<String, Chain>}
         */
        this.chains = new Map();

        if (arguments.length === 0) {
            return;
        }

        this.id = storageId;
        this.registrationId = address.deviceId;
        this.currentRatchet = {
            id: -1,
            rootKey: masterKey[0],
            ephemeralKeyPair: null,
            lastRemoteEphemeralKey,
            previousCounter: 0,
        };

        this.indexInfo = {
            remoteIdentityKey: remoteIdentity,
            closed: -1,
            baseKey: null,
            baseKeyType: null,
            address
        };
    }


    /**
     * Creates and adds a new chain object to the chain map
     * @param {Buffer} ephKey
     * @param {Buffer} key
     * @param {ChainType} chainType
     */
    create_chain(ephKey, key, chainType) {
        const _key = ephKey.toString('hex');
        this.chains.set(_key, {
            id: -1,
            /**@type {Map<number, Buffer>}*/
            messageKeys: new Map(),
            chainKey: {counter: -1, key: key},
            chainType,
        });
        return this.chains.get(_key);
    }

    /**
     * Gets a chain by its ephemeral key.
     * @param {Buffer} ephKey
     */
    get_chain(ephKey) {
        return this.chains.get(ephKey.toString('hex'));
    }

    /**
     * Gets and marks a chain for modification.
     * @param {Buffer} ephKey
     * @returns {Chain}
     */
    modify_chain(ephKey) {
        const chain = this.get_chain(ephKey);
        // If the chain id is -1 then it has yet to be inserted into
        // the database and does not require marking of modification.
        if ((chain?.id ?? -1) !== -1)
            chain.modified = true;
        return chain;
    }

    /**
     * Marks a chain for deletion by its ephemeral key.
     * @param {Buffer} ephKey
     */
    delete_chain(ephKey) {
        const chain = this.chains.get(ephKey);
        if (chain != null)
            chain.deleted = true;
    }

    static serialize_message_keys(chain) {
        const buffer = Buffer.alloc(chain.messageKeys.size * 36);
        let slice = buffer;
        for (const [key, value] of chain.messageKeys) {
            slice.writeUInt32BE(key);
            value.copy(slice, 4);
            slice = slice.slice(36);
        }
        return buffer;
    }

    static deserialize_message_keys(keys) {
        const map = new Map();
        while (keys.length >= 36) {
            const key = keys.readUInt32BE();
            const value = Buffer.alloc(32);
            keys.copy(value, 0, 4);
            map.set(key, value);
            keys = keys.slice(36);
        }
        return map;
    }

    /**
     * Creates a partial SignalSession instance,
     * from a row of data from a session table.
     * @param row
     * @returns {SignalSession}
     */
    static from_row(row) {
        const session = new SignalSession();
        session.id = row.id;
        session.registrationId = row.remoteDevice;
        session.indexInfo = {
            closed: row.closed,
            baseKey: row.baseKey,
            baseKeyType: row.baseKeyType,
            address: new SignalProtocolAddress(row.relayHost, row.remoteDevice),
            remoteIdentityKey: row.remoteIdentity,
        }
        session.currentRatchet = row.currentRatchet;
        return session;
    }

}

SessionBuilder.prototype = {
    /**
     * Creates a new Session object from the given
     * device info.
     * @param deviceInfo
     * @returns {SignalSession}
     */
    create_session: function (deviceInfo) {
        const validSignature = Internal.crypto.Ed25519Verify(
            deviceInfo.identityKey,
            deviceInfo.signedPreKey.keyPair.pubKey,
            deviceInfo.signedPreKey.signature,
        );

        if (!validSignature)
            throw new Error('Signature not verified');


        const baseKey = Internal.crypto.createKeyPair();

        const session = this.initSession(
            true, baseKey, undefined,
            deviceInfo.identityKey,
            deviceInfo.preKey?.publicKey,
            deviceInfo.signedPreKey.keyPair.pubKey,
            this.remoteAddress
        );

        this.storage.init_session(session);
        this.storage.commit_session_chains(session);

        session.pendingPreKey = {
            signedKeyId: deviceInfo.signedPreKey.keyId,
            baseKey: baseKey.pubKey
        };

        if (deviceInfo.preKey) {
            session.pendingPreKey.preKeyId = deviceInfo.preKey.keyId;
        }

        return session;
    },
    create_session_from_prekeys: function (keyInfo) {
        const session = this.initSession(
            false, keyInfo.preKey, keyInfo.signedPreKey,
            keyInfo.identityKey, keyInfo.baseKey, undefined,
            this.remoteAddress
        );

        this.storage.init_session(session);
        this.storage.commit_session_chains(session);

        return session;
    },
    initSession: function (isInitiator, ourEphemeralKey, ourSignedKey,
                           theirIdentityPubKey, theirEphemeralPubKey,
                           theirSignedPubKey, address) {
        const ourIdentity = this.storage.identity;
        if (isInitiator) {
            if (ourSignedKey !== undefined) {
                throw new Error("Invalid call to initSession");
            }
            ourSignedKey = ourEphemeralKey;
        } else {
            if (theirSignedPubKey !== undefined) {
                throw new Error("Invalid call to initSession");
            }
            theirSignedPubKey = theirEphemeralPubKey;
        }

        let sharedSecret;
        if (ourEphemeralKey === undefined || theirEphemeralPubKey === undefined) {
            sharedSecret = Buffer.alloc(32 * 4);
        } else {
            sharedSecret = Buffer.alloc(32 * 5);
        }

        for (let i = 0; i < 32; i++) {
            sharedSecret[i] = 0xff;
        }

        const ecRes = [
            Internal.crypto.ECDHE(theirSignedPubKey, ourIdentity.privKey),
            Internal.crypto.ECDHE(theirIdentityPubKey, Buffer.from(ourSignedKey.privKey)),
            Internal.crypto.ECDHE(theirSignedPubKey, ourSignedKey.privKey)
        ];

        if (isInitiator) {
            ecRes[0].copy(sharedSecret, 32);
            ecRes[1].copy(sharedSecret, 64);
        } else {
            ecRes[0].copy(sharedSecret, 64);
            ecRes[1].copy(sharedSecret, 32);
        }

        ecRes[2].copy(sharedSecret, 96);

        if (ourEphemeralKey !== undefined && theirEphemeralPubKey !== undefined) {
            const ecRes4 = Internal.crypto.ECDHE(theirEphemeralPubKey, ourEphemeralKey.privKey);
            ecRes4.copy(sharedSecret, 128);
        }

        const masterKey = Internal.crypto.HKDF(sharedSecret, Buffer.alloc(32), "WhisperText");
        const session = new SignalSession(masterKey,
            theirSignedPubKey, theirIdentityPubKey, address
        );

        // If we're initiating we go ahead and set our first sending ephemeral key now,
        // otherwise we figure it out when we first maybeStepRatchet with the remote's ephemeral key
        if (isInitiator) {
            session.indexInfo.baseKey = ourEphemeralKey.pubKey;
            session.indexInfo.baseKeyType = Internal.Record.BaseKeyType.OURS;
            session.currentRatchet.ephemeralKeyPair = Internal.crypto.createKeyPair();
            this.calculateSendingRatchet(session, theirSignedPubKey);
        } else {
            session.indexInfo.baseKey = theirEphemeralPubKey;
            session.indexInfo.baseKeyType = Internal.Record.BaseKeyType.THEIRS;
            session.currentRatchet.ephemeralKeyPair = ourSignedKey;
        }
        return session;
    },
    calculateSendingRatchet: function (session, remoteKey) {
        const ratchet = session.currentRatchet;
        const sharedSecret = Internal.crypto.ECDHE(remoteKey, ratchet.ephemeralKeyPair.privKey);
        const masterKey = Internal.crypto.HKDF(sharedSecret, Buffer.from(ratchet.rootKey), "WhisperRatchet");
        session.create_chain(ratchet.ephemeralKeyPair.pubKey, masterKey[1], Internal.Record.ChainType.SENDING);
        ratchet.rootKey = masterKey[0];
    },
};

module.exports = SessionBuilder;

module.exports.SignalSession = SignalSession;
