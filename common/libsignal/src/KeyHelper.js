let Internal = {
    crypto: require('./crypto.js').cryptoInternal,
};

//console.log(Internal);

function isNonNegativeInteger(n) {
    return (typeof n === 'number' && (n % 1) === 0 && n >= 0);
}

var KeyHelper = {
    generateIdentityKeyPair: async function () {
        return Internal.crypto.createKeyPair();
    },

    generateRegistrationId: function () {
        var registrationId = new Uint16Array(Internal.crypto.getRandomBytes(2))[0];
        return registrationId & 0x3fff;
    },

    /**
     *
     * @param {KeyPair} identityKeyPair
     * @param signedKeyId
     * @returns {*}
     */
    generateSignedPreKey: function (identityKeyPair, signedKeyId) {
        if (!Buffer.isBuffer(identityKeyPair.privKey) ||
            identityKeyPair.privKey.length !== 32 ||
            !Buffer.isBuffer(identityKeyPair.pubKey) ||
            identityKeyPair.pubKey.byteLength !== 33) {
            console.log(identityKeyPair);
            throw new TypeError('Invalid argument for identityKeyPair');
        }
        if (!isNonNegativeInteger(signedKeyId)) {
            throw new TypeError(
                'Invalid argument for signedKeyId: ' + signedKeyId
            );
        }

        const keyPair = Internal.crypto.createKeyPair();
        const sig = Internal.crypto.Ed25519Sign(identityKeyPair.privKey, keyPair.pubKey)
        return {
            keyId: signedKeyId,
            keyPair: keyPair,
            signature: sig
        };
    },

    generatePreKey: function (keyId) {
        if (!isNonNegativeInteger(keyId)) {
            throw new TypeError('Invalid argument for keyId: ' + keyId);
        }

        return {keyId: keyId, keyPair: Internal.crypto.createKeyPair()};
    }
};

module.exports = KeyHelper;
