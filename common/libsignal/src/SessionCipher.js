const Internal = {
    crypto: require('./crypto.js').cryptoInternal,
    SessionLock: require('./SessionLock.js'),
    Record: require('./SessionRecord.js'),
    SessionBuilder: require('./SessionBuilder.js'),
};

const SignalProtocolAddress = require('./SignalProtocolAddress');

function SessionCipher(storage, sessionOrAddress) {
    if (sessionOrAddress instanceof SignalProtocolAddress) {
        this.remoteAddress = sessionOrAddress;
    } else {
        this.session = sessionOrAddress;
        this.remoteAddress = sessionOrAddress.indexInfo.address;
    }

    this.storage = storage;
}

const util = require('util');

SessionCipher.prototype = {
    getRecord: function (encodedNumber) {
        return this.storage.loadSessions(encodedNumber).then(function (serialized) {
            if (serialized === undefined) {
                return undefined;
            }
            //console.log(serialized);
            return Internal.Record.SessionRecord.deserialize(serialized);
        });
    },
    encrypt: function (buffer, encoding) {
        if (!Buffer.isBuffer(buffer))
            buffer = Buffer.from(buffer);

        const store = this.storage;
        const ourIdentityKey = this.storage.identity;
        const session = this.session;
        const address = session.indexInfo.address;

        return Internal.SessionLock.queueJobForNumber(address.toString(), function () {
            const msg = {};

            let chain;
            msg.ephemeralKey = session.currentRatchet.ephemeralKeyPair.pubKey;

            chain = session.modify_chain(msg.ephemeralKey);
            if (chain == null) {
                throw new Error(`No chain was found for session - ${session.indexInfo.address}.`);
            }

            if (chain.chainType === Internal.Record.ChainType.RECEIVING) {
                throw new Error("Tried to encrypt on a receiving chain");
            }

            SessionCipher.prototype.fillMessageKeys(chain, chain.chainKey.counter + 1);

            let keys = Internal.crypto.HKDF(chain.messageKeys.get(chain.chainKey.counter),
                Buffer.alloc(32), "WhisperMessageKeys");
            chain.messageKeys.delete(chain.chainKey.counter)
            msg.counter = chain.chainKey.counter;
            msg.previousCounter = session.currentRatchet.previousCounter;

            msg.ciphertext = Internal.crypto.encrypt(keys[0], buffer, keys[2].slice(0, 16));
            const encodedMsg = msgToBuffer(msg);

            // Generate a HMAC value
            const macInput = Buffer.alloc(encodedMsg.byteLength + 33 * 2 + 1);
            ourIdentityKey.pubKey.copy(macInput)
            session.indexInfo.remoteIdentityKey.copy(macInput, 33);
            macInput[33 * 2] = (3 << 4) | 3;
            encodedMsg.copy(macInput, 33 * 2 + 1)

            let mac = Internal.crypto.sign(keys[1], macInput);


            const result = Buffer.alloc(encodedMsg.length + 9);
            result[0] = (3 << 4) | 3;
            encodedMsg.copy(result, 1)
            mac.slice(0, 8).copy(result, encodedMsg.length + 1);


            store.commit_session_chains(session);

            return {
                type: 1,
                body: result,
                registrationId: session.registrationId,
            };
        });
    },


    decryptWithSessionList: function (buffer, sessionList, errors) {
        // Iterate recursively through the list, attempting to decrypt
        // using each one at a time. Stop and return the result if we get
        // a valid result
        if (sessionList.length === 0) {
            return Promise.reject(errors[0]);
        }

        const session = sessionList.pop();
        return this.doDecryptWhisperMessage(buffer, session).then(function (plaintext) {
            return {plaintext: plaintext, session: session};
        }).catch(function (e) {
            if (e.name === 'MessageCounterError') {
                return Promise.reject(e);
            }

            errors.push(e);
            return this.decryptWithSessionList(buffer, sessionList, errors);
        }.bind(this));
    },
    decryptWhisperMessage: function (buffer, encoding) {
        return Internal.SessionLock.queueJobForNumber(this.remoteAddress.toString(), function () {
            return this.doDecryptWhisperMessage(buffer, this.session);
        }.bind(this));
    },
    doDecryptWhisperMessage: async function (messageBytes, session) {
        if (!(messageBytes instanceof Buffer)) {
            throw new Error("Expected messageBytes to be an Buffer");
        }

        const messageProto = messageBytes.slice(0, messageBytes.byteLength - 8);
        const mac = messageBytes.slice(messageBytes.byteLength - 8, messageBytes.byteLength);

        const message = decode_whisper_message(messageProto);

        if (session === undefined) {
            throw new Error("No session found to decrypt message from " + this.remoteAddress.toString());
        }
        if (session.indexInfo.closed !== -1) {
            console.log('decrypting message for closed session');
        }

        await this.maybeStepRatchet(session, message.ephemeralKey, message.previousCounter)

        const chain = session.modify_chain(message.ephemeralKey);
        if (chain.chainType === Internal.Record.ChainType.SENDING) {
            throw new Error("Tried to decrypt on a sending chain");
        }

        await SessionCipher.prototype.fillMessageKeys(chain, message.counter);
        const messageKey = chain.messageKeys.get(message.counter);
        if (messageKey === undefined) {
            const e = new Error("Message key not found. The counter was repeated or the key was not filled.");
            e.name = 'MessageCounterError';
            throw e;
        }
        chain.messageKeys.delete(message.counter);
        const keys = await Internal.crypto.HKDF(messageKey, Buffer.alloc(32), "WhisperMessageKeys");

        const ourIdentityKey = await this.storage.getIdentityKeyPair()

        const macInput = Buffer.alloc(messageProto.length + 33 * 2);
        session.indexInfo.remoteIdentityKey.copy(macInput);
        ourIdentityKey.pubKey.copy(macInput, 33);
        macInput[33 * 2] = (3 << 4) | 3;
        messageProto.slice(1).copy(macInput, 33 * 2 + 1);

        await Internal.crypto.verifyMAC(macInput, keys[1], mac, 8);
        const plaintext = Internal.crypto.decrypt(keys[0], message.ciphertext, keys[2].slice(0, 16));

        delete session.pendingPreKey;
        this.storage.commit_session_chains(session);
        this.storage.update_session_ratchet(session);

        return plaintext;
    },
    fillMessageKeys: function (chain, counter) {
        if (chain.chainKey.counter >= counter) {
            return Promise.resolve(); // Already calculated
        }

        if (counter - chain.chainKey.counter > 2000) {
            throw new Error('Over 2000 messages into the future!');
        }

        if (chain.chainKey.key === undefined) {
            throw new Error("Got invalid request to extend chain after it was already closed");
        }

        //const key = chain.chainKey.key;
        let key = chain.chainKey.key;
        const byteArray = Buffer.alloc(1);
        byteArray[0] = 1;
        const mac = Internal.crypto.sign(key, byteArray);
        byteArray[0] = 2;
        key = Internal.crypto.sign(key, byteArray);
        chain.messageKeys.set(chain.chainKey.counter + 1, mac);
        chain.chainKey.key = key;
        chain.chainKey.counter += 1;

        return this.fillMessageKeys(chain, counter)
    },
    maybeStepRatchet: function (session, remoteKey, previousCounter) {
        if (session.get_chain(remoteKey) != null) {
            return Promise.resolve();
        }

        const ratchet = session.currentRatchet;

        const previousRatchet = session.modify_chain(ratchet.lastRemoteEphemeralKey);
        if (previousRatchet != null) {
            this.fillMessageKeys(previousRatchet, previousCounter);
            delete previousRatchet.chainKey.key; // TODO This be an issue since the db requires it being not null.
            session.oldRatchetList.push({ // TODO this need a db update function
                added: Date.now(),
                ephemeralKey: ratchet.lastRemoteEphemeralKey,
            });
        }

        this.calculateRatchet(session, remoteKey, false);
        const previousRatchetEphKey = ratchet.ephemeralKeyPair.pubKey;
        if (session.get_chain(previousRatchetEphKey) !== undefined) {
            ratchet.previousCounter = session.get_chain(previousRatchetEphKey).chainKey.counter;
            session.delete_chain(previousRatchetEphKey);
        }

        ratchet.ephemeralKeyPair = Internal.crypto.createKeyPair();
        this.calculateRatchet(session, remoteKey, true);
        ratchet.lastRemoteEphemeralKey = remoteKey;
    },
    calculateRatchet: function (session, remoteKey, sending) {
        const ratchet = session.currentRatchet;

        const sharedSecret = Internal.crypto.ECDHE(remoteKey, ratchet.ephemeralKeyPair.privKey);
        const masterKey = Internal.crypto.HKDF(sharedSecret, ratchet.rootKey, "WhisperRatchet");

        let ephemeralPublicKey;
        if (sending) {
            ephemeralPublicKey = ratchet.ephemeralKeyPair.pubKey;
        } else {
            ephemeralPublicKey = remoteKey;
        }
        session.create_chain(ephemeralPublicKey, masterKey[1], sending ? Internal.Record.ChainType.SENDING : Internal.Record.ChainType.RECEIVING);

        ratchet.rootKey = masterKey[0];
    },
    getRemoteRegistrationId: function () {
        return Internal.SessionLock.queueJobForNumber(this.remoteAddress.toString(), function () {
            return this.getRecord(this.remoteAddress.toString()).then(function (record) {
                if (record === undefined) {
                    return undefined;
                }
                const openSession = record.getOpenSession();
                if (openSession === undefined) {
                    return null;
                }
                return openSession.registrationId;
            });
        }.bind(this));
    },
    hasOpenSession: function () {
        return Internal.SessionLock.queueJobForNumber(this.remoteAddress.toString(), function () {
            return this.getRecord(this.remoteAddress.toString()).then(function (record) {
                if (record === undefined) {
                    return false;
                }
                return record.haveOpenSession();
            });
        }.bind(this));
    },
    closeOpenSessionForDevice: function () {
        const address = this.remoteAddress.toString();
        return Internal.SessionLock.queueJobForNumber(address, function () {
            return this.getRecord(address).then(function (record) {
                if (record === undefined || record.getOpenSession() === undefined) {
                    return;
                }

                record.archiveCurrentState();
                return this.storage.storeSession(address, record.serialize());
            }.bind(this));
        }.bind(this));
    },
    deleteAllSessionsForDevice: function () {
        // Used in session reset scenarios, where we really need to delete
        const address = this.remoteAddress.toString();
        return Internal.SessionLock.queueJobForNumber(address, function () {
            return this.getRecord(address).then(function (record) {
                if (record === undefined) {
                    return;
                }

                record.deleteAllSessions();
                return this.storage.storeSession(address, record.serialize());
            }.bind(this));
        }.bind(this));
    }
};

module.exports = function (storage, remoteAddress) {
    const cipher = new SessionCipher(storage, remoteAddress);

    // returns a Promise that resolves to a ciphertext object
    this.encrypt = cipher.encrypt.bind(cipher);

    // returns a Promise that resolves to decrypted plaintext array buffer
    this.decryptWhisperMessage = cipher.decryptWhisperMessage.bind(cipher);

    this.getRemoteRegistrationId = cipher.getRemoteRegistrationId.bind(cipher);
    this.hasOpenSession = cipher.hasOpenSession.bind(cipher);
    this.closeOpenSessionForDevice = cipher.closeOpenSessionForDevice.bind(cipher);
    this.deleteAllSessionsForDevice = cipher.deleteAllSessionsForDevice.bind(cipher);
};


function msgToBuffer(msg) {
    let buffer = Buffer.alloc(msg.ephemeralKey.length + 16 + msg.ciphertext.length);
    buffer.writeUInt32BE(msg.counter);
    buffer.writeUInt32BE(msg.previousCounter, 4);
    buffer.writeUInt32BE(msg.ephemeralKey.length, 8);
    buffer.writeUInt32BE(msg.ciphertext.length, 12);
    Buffer.from(msg.ephemeralKey).copy(buffer, 16);
    msg.ciphertext.copy(buffer, 16 + msg.ephemeralKey.length);
    return buffer;
}

function decode_whisper_message(buffer) {
    const msg = {};
    buffer = buffer.slice(1); // Ignore the first byte

    msg.counter = buffer.readUInt32BE();
    msg.previousCounter = buffer.readUInt32BE(4);
    msg.ephemeralKey = Buffer.alloc(buffer.readUInt32BE(8));
    msg.ciphertext = Buffer.alloc(buffer.readUInt32BE(12));



    buffer = buffer.slice(16);
    buffer.copy(msg.ephemeralKey, 0, 0, msg.ephemeralKey.length);

    buffer = buffer.slice(msg.ephemeralKey.length);
    buffer.copy(msg.ciphertext);

    buffer = buffer.slice(msg.ciphertext.length);
    msg.hmac = Buffer.alloc(8);
    buffer.copy(msg.hmac);

    return msg;
}
