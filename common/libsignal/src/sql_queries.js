const util = {};
/**
 * @param query
 * @param params
 * @returns {Database.RunResult}
 */
util.runQuery = function runQuery(query, ...params) {
    return this.db.prepare(query).run(...params);
}

util.get = function get(query, ...params) {
    return this.db.prepare(query).get(...params);
}

util.getAll = function getAll(query, ...params) {
    return this.db.prepare(query).all(...params);
}

function bindUtil(target) {
    const boundUtil = {};
    for (const key in util) {
        boundUtil[key] = util[key].bind(target);
    }
    return boundUtil;
}

const queries = {util: bindUtil};

queries.initTables = `
create table if not exists kvs
(
    key varchar(16) not null
        constraint kv_pk
        primary key,
    value blob
);

create table if not exists keypairs
(
    id integer not null
        constraint keypairs_pk
        primary key autoincrement,
    name varchar(64) not null
        constraint keypairs_name
        unique,
    pubKey blob not null,
    privKey blob
);

create table if not exists sessions
(
  id integer not null
        constraint pkey
        primary key autoincrement,
  closed integer not null,
  currentRatchet integer
        constraint ratchet_fk
            references ratchets (id)
            on update cascade on delete set null,
  remoteIdentity blob not null,
  baseKey blob not null,
  baseKeyType integer not null,
  remoteDevice integer not null,
  relayHost varchar(255) not null,
  constraint remoteAddress
        unique (remoteDevice, relayHost)
);

create unique index if not exists idx_session_basekey
    on sessions (baseKey);

create table if not exists ratchets
(
    id integer not null
        constraint pkey
        primary key autoincrement,
    rootKey blob not null,
    ephemeralKey blob not null,
    lastRemoteEphemeralKey blob not null,
    previousCounter integer not null,
    session integer not null
        constraint ratchet_session_id_fk
            references sessions (id)
            on update cascade on delete cascade
);

create table if not exists devices
(
    id integer not null
        constraint pkey
        primary key autoincrement,
    identity blob not null,
    name varchar(64) not null
);

create table if not exists chains
(
    id integer not null
        constraint pkey
        primary key autoincrement,
    ephemeralKey blob not null,
    session integer not null
        constraint sessionId
        references sessions (id)
        on update cascade on delete cascade,
    messageKeys blob,
    key blob not null,
    counter integer not null,
    type integer not null
);

create unique index if not exists idx_chain_eph
    on chains (session, ephemeralKey);
`;

queries.insertKeys = "insert into keypairs (name, pubKey, privKey) values (?, ?, ?)";
queries.getKeysById = "select * from keypairs where id = ?";
queries.getKeysByName = "select * from keypairs where name = ?";
queries.deleteKeysById = "delete from keypairs where id = ?";
queries.deleteKeysByName = "delete from keypairs where name = ?";
queries.updateKeysWhereName = "update keypairs set pubKey = ?, privKey = ? where name = ?"
queries.upsertKeys = `
insert into keypairs (name, pubKey, privKey)
values (?1, ?2, ?3) on conflict(name) do
update set pubKey = ?2, privKey = ?3
`;

queries.getRatchetsByOwner = "select * from ratchets where session = ?";
queries.getRatchetById = "select * from ratchets where id = ?";
queries.insertRatchet = "insert into ratchets (rootKey, ephemeralKey, lastRemoteEphemeralKey, previousCounter, session) values (?,?,?,?,?)";
queries.updateRatchet = "update ratchets set rootKey = ?, ephemeralKey = ?, lastRemoteEphemeralKey = ?, previousCounter = ? where id = ?"

queries.getSessionById = "select * from sessions where id = ?";
queries.insertSession = "insert into sessions (closed, currentRatchet, remoteIdentity, baseKey, baseKeyType, remoteDevice, relayHost) values (?,null,?,?,?,?,?)";
queries.getSessionChains = `select * from chains where session = ?`;
queries.insertChain = "insert into chains (session, ephemeralKey, messageKeys, key, counter, type) values (?,?,?,?,?,?)";
queries.updateChain = "update chains set messageKeys = ?2, key = ?3, counter = ?4, type =?5 where id = ?1"
queries.deleteChain = "delete from chains where id = ?";
queries.getIdentityByAddress = `select remoteIdentity from sessions where remoteDevice = ? and relayHost = ?`
queries.getSessionByBaseKey = `select * from sessions where baseKey = ?`;
queries.getSession = `select * from sessions where remoteDevice= ? and relayHost =?`;

queries.getKvPair = `select * from kvs where key = ?`;
queries.setKvPair = `
insert into kvs (key, value)
values (?1, ?2) on conflict(key) do
update set value = ?2
`;

module.exports = queries;

