 /*
  * jobQueue manages multiple queues indexed by device to serialize
  * session io ops on the database.
  */
;(function() {
'use strict';

let SessionLock = {};
let jobQueue = {};

SessionLock.queueJobForNumber = function queueJobForNumber(number, runJob) {
    //console.log(number);
     let runPrevious = jobQueue[number] || Promise.resolve();
     let runCurrent = jobQueue[number] = runPrevious.then(runJob, runJob);
     runCurrent.then(function() {
         if (jobQueue[number] === runCurrent) {
             delete jobQueue[number];
         }
     });
     return runCurrent;
};


module.exports = SessionLock;

})();

