/* vims=4:sw=4 */


const crypto = require('crypto');
const curve = require('@p2/curve25519-native');

module.exports = {
    getRandomBytes: function (size) {
        return crypto.randomBytes(size);
    },
    encrypt: function (key, data, iv) { ////possible fix
        const cipher = crypto.createCipheriv('AES-256-CBC', key, iv);
        cipher.write(data);
        cipher.end();
        return cipher.read();
    },
    decrypt: function (key, data, iv) { ////possible fix
        const decipher = crypto.createDecipheriv('AES-256-CBC', key, iv);
        decipher.write(data);
        decipher.end();
        return decipher.read();
    },
    sign: function (key, data) { ////possible fix
        // Think we need to create a hmac rather than a sign
        // object since they use hmac in the original code.
        // Not sure though.
        const hmac = crypto.createHmac('SHA256', key);
        hmac.write(data);

        hmac.end();
        return Buffer.from(hmac.read());
    },

    hash: function (data) {
        const hash = crypto.createHash('SHA512');
        hash.write(data);
        return hash.read();
    },

    // Security concern: The implementation itself does not seem to be problematic
    // however most usages of the function seem to use the same info ("WhisperMessageKeys"
    // and salt (A buffer of 32 zero-values).
    HKDF: function (input, salt, info) {
        if (salt.length !== 32) {
            throw new Error("Got salt of incorrect length");
        }

        if (!Buffer.isBuffer(info)) info = Buffer.from(info);

        // Specific implementation of RFC 5869 that only returns the first 3 32-byte chunks
        // TODO: We dont always need the third chunk, we might skip it


        const PRK = module.exports.sign(salt, input);
        const infoBuffer = Buffer.alloc(Buffer.byteLength(info, 'utf8') + 33);
        (Buffer.from(info)).copy(infoBuffer, 32);
        infoBuffer[infoBuffer.length - 1] = 1;

        const T1 = module.exports.sign(PRK, infoBuffer.slice(32));
        T1.copy(infoBuffer);
        infoBuffer[infoBuffer.length - 1] = 2;

        const T2 = module.exports.sign(PRK, infoBuffer);
        T2.copy(infoBuffer)
        infoBuffer[infoBuffer.length - 1] = 3;

        const T3 = module.exports.sign(PRK, infoBuffer)
        return [T1, T2, T3];
    },

    /*
    The below four functions originally return
    values returned by functions `Internal.Curve.async`
    "namespace". They may need to return values by promise.
    I am not sure though so beware when testing.
     */

    // Curve 25519 crypto
    createKeyPair: function (privKey) {
        if (privKey === undefined) {
            privKey = crypto.randomBytes(32);
        }

        return curve.createKeyPair(privKey);
    },
    ECDHE: function (pubKey, privKey) {
        return curve.ECDHE(pubKey, privKey);
    },
    Ed25519Sign: function (privKey, message) {
        return curve.ed25519Sign(privKey, message);
    },
    Ed25519Verify: function (pubKey, msg, sig) {
        return curve.ed25519Verify(pubKey, msg, sig);
    },
};

module.exports.verifyMAC = function (data, key, mac, length) {
    const calculated_mac = module.exports.sign(key, data);
    if (mac.length !== length || calculated_mac.length < length)
        throw new Error("Bad MAC length");

    let result = 0;

    for (let i = 0; i < mac.length; ++i)
        result = result | (calculated_mac[i] ^ mac[i]);

    if (result !== 0)
        throw new Error("Bad MAC");
};

module.exports.HKDF.deriveSecrets = function (input, salt, info) {
    return module.exports.HKDF(input, salt, info);
};

module.exports.calculateMAC = function (key, data) {
    return module.exports.sign(key, data);
};

// For backwards compatibility.
module.exports.cryptoInternal = module.exports;
