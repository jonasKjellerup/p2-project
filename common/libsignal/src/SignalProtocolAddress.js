class SignalProtocolAddress {
  // We add a reference to the class itself.
  // This is done to make the restructuring backwards
  // compatible with code that expects it to located at
  // modules.exports.SignalProtocolAddress
  // (a.k.a libsignal.SignalProtocolAddress)
  static SignalProtocolAddress = SignalProtocolAddress;

  constructor(relayHost, deviceId) {
    this.relayHost = relayHost;
    this.deviceId = deviceId;
  }

  getName() {
    return this.relayHost;
  }

  getDeviceId() {
    return this.deviceId;
  }

  toString() {
    return `${this.deviceId}@${this.relayHost}`;
  }

  equals(other) {
    if (!(other instanceof SignalProtocolAddress)) { return false; }
    return other.relayHost === this.relayHost && other.deviceId === this.deviceId;
  }

  static fromString(encodedAddress) {
    if (typeof encodedAddress !== 'string' || !encodedAddress.match(/\d+@.*/)) {
      throw new Error('Invalid SignalProtocolAddress string');
    }
    const parts = encodedAddress.split('@');
    return new SignalProtocolAddress(parts[1], parseInt(parts[0]));
  }
}

module.exports = SignalProtocolAddress;
