const sqlite3 = require('better-sqlite3');
const queries = require('./sql_queries');
const SignalProtocolAddress = require('./SignalProtocolAddress');
const {SignalSession} = require('./SessionBuilder');

class SignalStore {
    static Direction = {
        SENDING: 1,
        RECEIVING: 2,
    };

    /**
     * Gets the identity keys of the store owner.
     * @returns {KeyPair}
     */
    get identity() {
        return this.getKeyPair(`identity-self`);
    }

    /**
     * Sets the identity key.
     */
    set identity(pair) {
        return this.util.runQuery(queries.upsertKeys, {1: `identity-self`, 2: pair.pubKey, 3: pair.privKey});
    }

    constructor(dbPath) {
        this.db = sqlite3(dbPath);
        this.db.exec(queries.initTables);
        this.util = queries.util(this);
    }

    /**
     * Gets a key/value pair.
     * @param {String} key
     * @returns {{key: String, value: *}}
     */
    getValue(key) {
        return this.util.get(queries.getKvPair, key);
    }

    /**
     * Inserts or updates a key value pair.
     * @param {String} key
     * @param {*} value
     */
    setValue(key, value) {
        this.util.runQuery(queries.setKvPair, {1: key, 2: value});
    }

    /**
     * Gets a key-pair by name or id.
     * @param {number|String} key
     * @returns {KeyPair}
     */
    getKeyPair(key) {
        return this.util.get(
            typeof key === 'number' ? queries.getKeysById : queries.getKeysByName,
            key
        );
    }

    /**
     * Inserts a new key-pair.
     * @param {String} name
     * @param {KeyPair} pair
     * @returns {number}
     */
    insertKeyPair(name, pair) {
        const result = this.util.runQuery(queries.insertKeys, name, pair.pubKey, pair.privKey);
        return result.lastInsertRowid;
    }

    /**
     * Delete a key-pair from storage.
     * @param {String} name
     */
    deleteKeyPair(name) {
        this.util.runQuery(queries.deleteKeysByName, name);
    }

    /**
     * Replaces the key-pair stored by the given name.
     * @param {String} name
     * @param {KeyPair} keys
     */
    updateKeyPair(name, keys) {
        this.util.runQuery(queries.updateKeysWhereName, keys.pubKey, keys.privKey, name);
    }

    /**
     * Initialises a storage entry for the given session.
     * @param {SignalSession} session
     */
    init_session(session) {
        session.id = this.util.runQuery(queries.insertSession,
            session.indexInfo.closed,
            session.indexInfo.remoteIdentityKey,
            session.indexInfo.baseKey,
            session.indexInfo.baseKeyType,
            session.indexInfo.address.deviceId,
            session.indexInfo.address.relayHost).lastInsertRowid;

        session.currentRatchet.id = this.util.runQuery(queries.insertRatchet,
            session.currentRatchet.rootKey,
            Buffer.concat([
                session.currentRatchet.ephemeralKeyPair.pubKey,
                session.currentRatchet.ephemeralKeyPair.privKey,
            ]),
            session.currentRatchet.lastRemoteEphemeralKey,
            session.currentRatchet.previousCounter,
            session.id).lastInsertRowid;

        this.util.runQuery("update sessions set currentRatchet = ? where id = ?", session.currentRatchet.id, session.id);
    }

    /**
     * Commits any changes made to a sessions chains
     * to the storage system.
     * @param {SignalSession} session
     */
    commit_session_chains(session) {
        if (session.id === -1)
            throw new Error("Session does not exist in database. Commit the session before committing the session chains.");
        for (const [key, chain] of session.chains) {
            if (chain.id === -1) {
                chain.id = this.util.runQuery(queries.insertChain,
                    session.id, Buffer.from(key, 'hex'),
                    SignalSession.serialize_message_keys(chain),
                    chain.chainKey.key, chain.chainKey.counter, chain.chainType
                ).lastInsertRowid;
            } else if (chain.modified) {
                this.util.runQuery(queries.updateChain,
                    {
                        1: chain.id, 2: SignalSession.serialize_message_keys(chain),
                        3: chain.chainKey.key, 4: chain.chainKey.counter, 5: chain.chainType
                    }
                );
                chain.modified = false;
            } else if (chain.delete) {
                this.util.runQuery(queries.deleteChain, chain.id);
                session.chains.delete(key);
            }
        }
    }

    load_session_chains(session) {
        const chains = this.util.getAll(queries.getSessionChains, session.id)
            .map(row => {
                if (row.messageKeys != null)
                    row.messageKeys = SignalSession.deserialize_message_keys(row.messageKeys);
                else row.messageKeys = new Map();
                row.chainKey = {counter: row.counter, key: row.key};
                row.chainType = row.type;
                delete row.type;
                delete row.key;
                delete row.counter;
                const key = row.ephemeralKey;
                delete row.ephemeralKey;
                return [key.toString('hex'), row];
            });
        session.chains = new Map(chains);
    }

    load_session_ratchet(session) {
        session.currentRatchet = this.util.get(queries.getRatchetById, session.currentRatchet);
        if (session.currentRatchet.ephemeralKey != null) {
            const serialized = session.currentRatchet.ephemeralKey;
            session.currentRatchet.ephemeralKeyPair = {
                pubKey: Buffer.alloc(33),
                privKey: Buffer.alloc(32),
            };
            serialized.copy(session.currentRatchet.ephemeralKeyPair.pubKey, 0, 0, 33);
            serialized.copy(session.currentRatchet.ephemeralKeyPair.privKey, 0, 33);
            delete session.currentRatchet.ephemeralKey;
        }
    }

    update_session_ratchet(session) {
        const ratchet = session.currentRatchet;
        this.util.runQuery(queries.updateRatchet,
            ratchet.rootKey,
            Buffer.concat([
                ratchet.ephemeralKeyPair.pubKey,
                ratchet.ephemeralKeyPair.pubKey
            ]),
            ratchet.lastRemoteEphemeralKey,
            ratchet.previousCounter,
            ratchet.id);
    }

    add_old_ratchet(session, ratchet) {
        // TODO
    }


    close() {
        this.db.close();
    }

    /*
    Reimplementation of preexisting store interface functions.
     */

    getIdentityKeyPair() {
        return this.identity;
    }


    async isTrustedIdentity(id, key) {
        const trusted = await this.getKeyPair(`identity-${id}`);
        if (trusted == null)
            return true;
        return Buffer.compare(trusted.pubKey, key) === 0;
    }

    async loadIdentityKey(id) {
        const address = SignalProtocolAddress.fromString(id);
        return (await this.getKeyPair(`identity-${address.getName()}`)).pubKey;
    }

    async saveIdentity(id, key) {
        const address = SignalProtocolAddress.fromString(id);
        const existing = await this.getKeyPair(`identity-${address.getName()}`);

        if (existing) {
            if (buffer.compare(key, existing.pubKey) !== 0) {
                await this.updateKeyPair(`identity-${address.getName}`, {privKey: null, pubKey: key});
                return true;
            }
        } else {
            await this.insertKeyPair(`identity-${address.getName()}`, {privKey: null, pubKey: key});
        }

        return false

    }

    /**
     * Gets an array containing all sessions.
     * @returns {SignalSession[]}
     */
    load_sessions() {
        const sessionInfo = this.util.getAll("select * from sessions");

        return sessionInfo.map(row => {
            const session = SignalSession.from_row(row);
            this.load_session_ratchet(session);
            this.load_session_chains(session);
            // TODO load old ratchets
            return session;
        });
    }

    /**
     * Gets a specific session based on the given signal address.
     * @param {SignalProtocolAddress} address
     * @returns {SignalSession}
     */
    load_session(address) {
        let session = this.util.get(queries.getSession, address.deviceId, address.relayHost);
        if (session) {
            session = SignalSession.from_row(session);
            this.load_session_ratchet(session);
            this.load_session_chains(session);
        }
        return session;
    }
}

module.exports = SignalStore;
