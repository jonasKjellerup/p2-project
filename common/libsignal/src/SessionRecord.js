/*
 * vim: ts=4:sw=4
 */
let Internal = {};
const util = require('util');

Internal.BaseKeyType = {
  OURS: 1,
  THEIRS: 2
};
Internal.ChainType = {
  SENDING: 1,
  RECEIVING: 2
};

const ARCHIVED_STATES_MAX_LENGTH = 40;
const OLD_RATCHETS_MAX_LENGTH = 10;
const SESSION_RECORD_VERSION = 'v1';

class SessionRecord {
    constructor(sessions={}) {
        this.sessions = sessions;
    }

    haveOpenSession() {
        const openSession = this.getOpenSession();
        return typeof openSession?.registrationId === 'number';
    }

    /**
     * @param {Buffer} baseKey
     */
    getSessionByBaseKey(baseKey) {
        const session = this.sessions[baseKey.toString('hex')]
        if (session?.indexInfo.baseKeyType === Internal.BaseKeyType.OURS) {
            console.log("Tried to lookup a session using our basekey");
            return undefined;
        }
        return session
    }

    getOpenSession() {
        if (this.sessions === undefined)
            return undefined;

        this.detectDuplicateOpenSessions();

        for (const key in this.sessions) {
            if (this.sessions[key].indexInfo.closed === -1) {
                return this.sessions[key]
            }
        }

        return undefined;
    }

    updateSessionState(session) {
        SessionRecord.removeOldChains(session);
        this.sessions[session.indexInfo.baseKey.toString('hex')] = session;
        this.removeOldSessions();
    }

    detectDuplicateOpenSessions() {
        let openSession;
        for (const key in this.sessions) {
            if (this.sessions[key].indexInfo.closed === -1) {
                if (openSession !== undefined) {
                    throw new Error("Datastore inconsistensy: multiple open sessions");
                }
                openSession = this.sessions[key];
            }
        }
    }

    removeOldSessions() {
        let oldestBaseKey, oldestSession;
        while (Object.keys(this.sessions).length > ARCHIVED_STATES_MAX_LENGTH) {
            for (const key in sessions) {
                const session = this.sessions[key];
                if (session.indexInfo.closed > -1 && // session is closed
                    (!oldestSession || session.indexInfo.closed < oldestSession.indexInfo.closed)) {
                    oldestBaseKey = key;
                    oldestSession = session;
                }
            }
            console.log("Deleting session closed at", oldestSession.indexInfo.closed);
            delete this.sessions[util.toString(oldestBaseKey)];
        }
    }

    getSessions() {
        let list = [];
        let openSession;
        for (const key in this.sessions) {
            if (this.sessions[key].indexInfo.closed === -1) {
                openSession = this.sessions[key];
            } else {
                list.push(this.sessions[key]);
            }
        }

        list = list.sort((s1,s2) => s1.indexInfo.closed - s2.indexInfo.closed);
        if (openSession) {
            list.push(openSession)
        }
        return list;
    }

    archiveCurrentState() {
        const open_session = this.getOpenSession();
        if (open_session != null) {
            console.log('closing session');
            open_session.indexInfo.closed = Date.now();
            this.updateSessionState(open_session);
        }
    }

    deleteAllSessions() {
        this.sessions = {};
    }

    static promoteState(session) {
        console.log('promoting session');
        session.indexInfo.closed = -1;
    }

    static removeOldChains(session) {
        // Sending ratchets are always removed when we step because we never need them again
        // Receiving ratchets are added to the oldRatchetList, which we parse
        // here and remove all but the last ten.
        while (session.oldRatchetList.length > OLD_RATCHETS_MAX_LENGTH) {
            let index = 0;
            let oldest = session.oldRatchetList[0];
            for (let i = 0; i < session.oldRatchetList.length; i++) {
                if (session.oldRatchetList[i].added < oldest.added) {
                    oldest = session.oldRatchetList[i];
                    index = i;
                }
            }
            console.log("Deleting chain closed at", oldest.added);
            session.delete_chain(oldest.ephemeralKey);
            session.oldRatchetList.splice(index, 1);
        }
    }

    serialize() {
        return JSON.stringify(this.sessions);
    }

    static deserialize(source) {
        const sessions = JSON.parse(source);
        if (typeof sessions !== 'object' || Array.isArray(sessions))
            throw new Error("Error deserializing SessionRecord");

        return new SessionRecord(sessions);
    }
}

Internal.SessionRecord = SessionRecord;

module.exports = Internal;
