/*
    Type definitions for Signal objects.
    Exists mainly as a reference for the
    expected contents of the various library objects.
 */

import SignalProtocolAddress from './src/SignalProtocolAddress';
export const SignalProtocolAddress = SignalProtocolAddres;

export enum BaseKeyType {
    OURS = 1,
    THEIRS,
}

export enum ChainType {
    SENDING = 1,
    RECEIVING,
}

export interface KeyPair {
    pubKey: Buffer;
    privKey: Buffer;
}

export interface Chain {
    messageKeys: Map<number, Buffer>;
    chainKey: { counter: number, key: Buffer};
    chainType: ChainType;
    /* Extra fields for database management*/
    id: number;
    modified?: boolean;
    delete?: boolean;
}

export interface Ratchet {
    rootKey: Buffer;
    ephemeralKeyPair: KeyPair;
    lastRemoteEphemeralKey: Buffer;
    previousCounter: number;
}

export interface IndexInfo {
    remoteIdentityKey: Buffer;
    baseKey: Buffer;
    baseKeyType: BaseKeyType;
    closed: number;
}

export interface SignalSession {
    registrationId: number;
    currentRatchet: Ratchet;
    indexInfo: IndexInfo;
    oldRatchetList: Ratchet[];
    chains: Map<string, Chain>;
}
