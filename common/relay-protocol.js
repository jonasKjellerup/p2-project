/**
 * The `relay-protocol` module defines implementation constants,
 * utility functions and a reference client implementation of
 * the relay protocol.
 *
 * @module relay-protocol
 */

const {Transform} = require('stream');
const tls = require('tls');
const curve25519 = require('@p2/curve25519-native');

/**
 * A packet object represents the smallest complete
 * piece of information that the relay protocol accepts.
 *
 * When serialized to a Buffer, fields are laid out as follows:
 * ```
 * |     0     |      1-2     |   3..   |
 * | packet id | payload size | payload |
 * ```
 *
 * All integer types are written in Network order (Big-Endian).
 *
 * @property {number} id - A numeric value from 0 to 255 (inclusive). See packet* constants for valid accepted values.
 * @property {number} payloadSize - A numeric value from 0 to 65535. Should match the actual length of the payload Buffer.
 * @property {Buffer} payload - A buffer with length equal to payloadSize.
 */
class Packet {
    constructor(id, payload) {
        if (id > 255 || id < 0)
            throw new RangeError("Given `id` in create_packet call is out of range (valid range = 0..256).");
        if (!(payload instanceof Buffer) && payload !== null)
            throw new TypeError("Expected given `payload` to be an instance of `Buffer`.")
        if ((payload?.length ?? 0) > maxPayloadSize)
            throw new RangeError("Given `payload` exceeds maximum length of 65535.")

        this.id = id;
        this.payloadSize = payload?.length ?? 0;
        this.payload = payload;
    }

    /**
     * Serializes the packet object as a buffer.
     * @returns {Buffer}
     * @throws If the payload length specified by `this.payloadLength` does not match
     * the length of the `this.payload` buffer an error will be thrown.
     */
    serialize() {
        const buffer = Buffer.alloc(Packet.headerSize + this.payloadSize);
        if (this.payloadSize !== 0) {
            if (this.payload?.length !== this.payloadSize)
                throw new Error("payload buffer length does not match given length");
            this.payload.copy(buffer, 3);
        }

        buffer[0] = this.id;
        buffer.writeUInt16BE(this.payloadSize, 1);
        return buffer;
    }

    static serialize_parts(id, payload) {
        return (new Packet(id, payload)).serialize();
    }

    /**
     * Allocates and initialises a buffer for a zero-sized packet.
     * This does not work for `responsePacket` since a zero-sized
     * response must also include a response code. For zero-sized
     * response packets see {@link create_response}.
     * @param id - The packet id that is to be used.
     * @returns {Buffer}
     */
    static create_zero_sized(id) {
        const buffer = Buffer.alloc(Packet.headerSize);
        buffer[0] = id;
        return buffer;
    }
}

const parserStates = {
    readId: -1,
    readSize: 0,
    readPayload: 1,
};

const errorWaitHandleAlreadyInUse = "Wait handle already in use.";

/**
 * A transport stream that consumes a series of
 * bytes converting them into `Packet` objects.
 */
class PacketParser extends Transform {
    workPacket = {
        id: packetNone,
        payloadSize: -1,
        payload: null,
    };

    /**
     * Internal buffer used to store incomplete work.
     * @type {Buffer|number}
     */
    workBuffer = null;

    parserState = parserStates.readId;

    packetQueue = [];

    waitHandle = null;

    async* [Symbol.asyncIterator]() {
        while (true) {
            yield await this.nextPacket;
        }
    }

    /**
     * A promise for the next packet.
     * If the next packet has already
     * been taken then an error is thrown.
     * @returns {Promise<Packet>}
     */
    get nextPacket() {
        // If there are packages in the queue return them first.
        if (this.packetQueue.length > 0)
            return Promise.resolve(this.packetQueue.shift());

        // If the wait handle is taken reject.
        if (this.waitHandle != null)
            return Promise.reject(new Error(errorWaitHandleAlreadyInUse));

        // Otherwise return a wait handle.
        const self = this;
        return new Promise(resolve => self.waitHandle = resolve)
            .then(() => self.packetQueue.shift());
    };

    constructor() {
        super({readableObjectMode: true});

        this.on('data', function (packet) {
            this.packetQueue.push(packet);
            if (this.waitHandle != null) {
                this.waitHandle();
                this.waitHandle = null;
            }
        });
    }

    setReadPayloadState() { // TODO maybe give this a more appropriate name
        if (this.workPacket.payloadSize > 0)
            this.parserState = parserStates.readPayload;
        else {
            this.push(new Packet(this.workPacket.id, this.workPacket.payload));
            this.workPacket = {id: 0, payloadSize: -1, payload: null};
            this.workBuffer = null;
            this.parserState = parserStates.readId
        }
    }

    _transform(chunk, encoding, callback) {
        if (Buffer.isBuffer(this.workBuffer)) {
            chunk = Buffer.concat([this.workBuffer, chunk]);
            this.workBuffer = null;
        }

        while (chunk.length > 0) {
            if (this.parserState === parserStates.readId) {
                this.workPacket.id = chunk[0];
                chunk = chunk.slice(1);

                this.parserState = parserStates.readSize;
            } else if (this.parserState === parserStates.readSize) {
                if (typeof this.workBuffer === 'number') {
                    this.workPacket.payloadSize = Buffer.from([this.workBuffer, chunk[0]]).readUInt16BE();
                    this.workBuffer = null;

                    this.setReadPayloadState();
                    chunk = chunk.slice(1);
                    continue;
                } else if (chunk.length < 2) {
                    this.workBuffer = chunk[0];
                    chunk = chunk.slice(1);
                    continue;
                }

                this.workPacket.payloadSize = chunk.readUInt16BE();
                this.setReadPayloadState();
                chunk = chunk.slice(2);
            } else {
                let remaining = this.workPacket.payloadSize - (this.workBuffer?.length ?? 0);

                let payloadChunk = chunk.slice(0, remaining);
                let remainingChunk = chunk.slice(remaining);

                remaining -= payloadChunk.length;

                if (this.workBuffer == null)
                    this.workBuffer = payloadChunk;
                else
                    this.workBuffer = Buffer.concat([this.workBuffer, payloadChunk]);

                if (remaining === 0) {
                    this.workPacket.payload = this.workBuffer;
                    this.push(new Packet(this.workPacket.id, this.workPacket.payload));
                    this.workPacket = {id: 0, payloadSize: -1, payload: null};
                    this.workBuffer = null;
                    this.parserState = parserStates.readId
                }

                chunk = remainingChunk;
            }
        }

        callback();
    }
}

/**
 * The size of a packet header.
 * @readonly
 * @type {number}
 */
Packet.headerSize = 3;

/**
 * Packet id representing a non-existing packet.
 * If the relay receives a packet marked with
 * `packetNone` it will be ignored completely.
 * @type {number}
 */
const packetNone = 0;

/**
 * Defines a packet that is sent as a series of smaller packets.
 * This is useful when a message is too large for a single packet.
 *
 * **Packet information:**
 *
 * First the sender will announce that the upcoming message
 * will be sent in multiple parts. This is announcement
 * must contain the number of parts that will be sent.
 *
 * ```
 * Expected payload size    = 2
 * payload[0..1]            = <part count>
 * ```
 *
 * The sender should then pack the data into <part count> packets
 * marked by the appropriate id and payload size. The payload
 * of all packets except the last one must be filled entirely.
 * Meaning that all 65535 bytes are used.
 *
 * Once all packets have been sent and received communication may
 * resume as usual.
 *
 * @type {number}
 */
const packetMultipart = 1;

/**
 * Used as a generic response packet. Packet information:
 *
 * ```
 * Payload[0]          = <response code>
 * Payload[..]         = <generic content>*
 * ```
 *
 * Besides the response code the contents of the payload are
 * entirely dependent on the context in which it is send
 * (e.g. the packet that it is responding to).*
 * See the response section of the relevant packet identifier
 * documentation.
 * @type {number}
 */
const packetResponse = 2;

/**
 * Used when establishing a TLS session. Packet information:
 *
 * The exact packet layout various depending on the session type specifier
 * given by the sender. Likewise the response from the server will differ
 * in response to the session type. The specifics for each session type
 * are listed below.
 *
 * ```
 * if <Session type specifier> = sessionNew:
 *   Initial request (sessionInit packet):
 *     Payload[0]      = <Session type specifier>
 *     Payload[1-2]    = <Identity key size>
 *     Payload[3]      = <Remote count>
 *     Payload[4..]    = <Identity key>
 *     Payload[..]     = <Remote info>
 *
 *   Response #1 <device was registered successfully) (response<Ok> packet):
 *     Response[0-3]    = <Hub-device id>
 *     Response[..]<4 * remote count>  = <remote-device id>
 *
 * if <Session type specifier> = sessionHub || sessionRemote:
 *  >Initial request (sessionInit packet):
 *     Payload[1-4]    = <Device-id>
 *
 *  <Server response #1 <device was found> (response<Ok> packet):
 *     When a session is started the relay will first respond with a identity challenge
 *     must be solved by the client. The challenge is created by encrypting a series of
 *     random bytes with the clients public identity key.
 *
 *     Response[..]      = <Challenge>
 *
 *  >Client response #1 (response<Ok> packet):
 *     Since the challenge is encrypted with the public key the client
 *     must decrypt it using its private key.
 *
 *     Response[..]     = <Solution>
 *
 *  >Server response #2 <solution was correct> (response<Ok> packet - zero-sized)
 * ```
 */
const packetSessionInit = 3;

/**
 * Used by the client to poll for incoming messages.
 *
 * **Packet information:**
 * ```
 * Expected payload size = 0
 * Requires authorization
 *
 * >Server response #1 <no messages found> (response<Ok> packet - zero-sized)
 *
 * >Server response #1 <messages found & size <= maxPayloadSize - 1> (response<Ok> packet)
 *     Response[..]     = <Messages>
 *
 * >Server response #1 <messages found & size >= maxPayloadSize> (multipart packet)
 *  If there is more message data than can be sent in a single payload
 *  we announce that it will be sent in multiple parts. Then we send the
 *  parts.
 *  >Server response #1.x (response<Ok> packet)
 *     Response[..]     = <payload part>
 *
 * <Client response #1 <messages received successfully> (Response<Ok> packet - zero-sized)
 *  If the response was successfully received then
 *  the messages will be removed from the database.
 *
 * <Client response #1 <failure receiving messages> (Response<Failure> - zero-sized)
 * ```
 */
const packetPoll = 4;

const packetSend = 5;

const packetAuthRemote = 6;

/**
 * The maximum size of packet payload.
 *
 * The length of a payload is specified by a 16-bit
 * integer, hence the limit of (1<<16)-1.
 * @type {number}
 */
const maxPayloadSize = (1 << 16) - 1;

/*
    Session type specifiers. These are used in session init packets
    to specify the device/session relationship.
    Note: session in this context "session" refers to the TLS connection/session
        not a signal session.
    */

/// Used when registering a new local-hub.
const sessionNew = 1;
/// Used for establishing a session between sessionHub and relay.
const sessionHub = 2;
// Used for establishing a session between remote and relay.
const sessionRemote = 3;

/*
    Response codes. Used to identify success state of
    the operation in response to which the response was sent.
 */

/**
 * Marks a response to a successful/valid operation.
 * The response structure is entirely context dependent
 * see documentation of specific packets for information
 * on structure and usage.
 * @type {number}
 */
const responseOk = 1;

/**
 * Marks a response to a unsuccessful operation.
 * Like the case of `responseOk` the contents of
 * the response are context dependent.
 * @type {number}
 */
const responseFailure = 2;

/**
 * A response used to inform the end-client of invalid packet usage.
 * An example case where this response may be received is, when
 * trying to perform an authorized operation through an unauthorized
 * session.
 *
 * The response consists of a single string that specifies
 * the reason for the invalidity of the packet.
 * @type {number}
 */
const responseInvalidPacket = 3;

const responseError = 4;


const headerSize = 3;

/**
 * Creates a serialized packet from the given payload and id.
 * @param id - An id in the range of 0 to 255 (inclusive).
 * @param {Buffer} payload
 * @return {Buffer}
 * @deprecated Use `Packet.serialize_parts`
 */
function create_packet(id, payload) {
    if (id > 255 || id < 0)
        throw new RangeError("Given `id` in create_packet call is out of range (valid range = 0..256).");
    if (!(payload instanceof Buffer))
        throw new TypeError("Expected given `payload` to be an instance of `Buffer`.")
    if (payload.length > maxPayloadSize)
        throw new RangeError("Given `payload` exceeds maximum length of 65535.")

    const header = Buffer.alloc(headerSize);
    header[0] = id;
    header.writeUInt16BE(payload.length, 1);

    return Buffer.concat([header, payload]);
}

/**
 * Creates a response packet.
 * @param responseCode
 * @param content
 * @returns {Buffer}
 */
function create_response(responseCode, content = Buffer.alloc(0)) {
    if (!Buffer.isBuffer(content)) {
        content = Buffer.from(content);
    }

    let payload = Buffer.alloc(1 + content.length);
    payload[0] = responseCode;
    content.copy(payload, 1);

    return create_packet(packetResponse, payload);
}

/**
 * An error type used whenever a protocol error is encountered
 */
class ProtocolError extends Error {
    static deserializationError = 1;
    static packetFormatError = 2;
    static unexpectedResponseError = 3;
    static relayError = 4;
    /**
     * A table that maps numeric error kind values to
     * a string representation.
     * @type {Object.<number,string>}
     */
    static errorKindNames = {
        [ProtocolError.deserializationError]: "DeserializationError",
        [ProtocolError.packetFormatError]: "PacketFormatError",
        [ProtocolError.unexpectedResponseError]: "UnexpectedResponseError",
        [ProtocolError.relayError]: "RelayError"
    };

    constructor(kind, msg) {
        super(msg)
        this.kind = kind;
        this.name = ProtocolError.errorKindNames[kind];
    }
}

/**
 * Represents a message sent through the relay (through use of send/poll).
 */
class Message {
    constructor(recipient, sender, contents) {
        this.recipient = recipient;
        this.sender = sender;
        this.contents = contents;
    }

    /**
     * Deserializes a message buffer.
     * @param buffer
     * @returns {{end: number, message: Message}|null}
     * An object containing a {@link Message} object and the offset for
     * the next byte past the last byte of the message contents.
     * Null is returned if the given buffer is null.
     * @throws When the buffer cannot be deserialized (incomplete header or contents).
     */
    static from_buffer(buffer) {
        if (buffer.length === 0)
            return null;
        if (buffer.length < 10)
            throw new ProtocolError(ProtocolError.deserializationError, "Incomplete message header.");

        const recipient = buffer.readUInt32BE();
        const sender = buffer.readUInt32BE(4);
        const size = buffer.readUInt16BE(8);

        if (buffer.length - 10 < size)
            throw new ProtocolError(ProtocolError.deserializationError,
                `Incomplete message contents. Expected content length = ${size} bytes. Received ${buffer.length - 10} bytes.`)

        return {
            message: new Message(recipient, sender, buffer.slice(10, 10 + size)),
            end: 10 + size
        }
    }

}

const isLocked = Symbol("relay client lock state");
const accessQueue = Symbol("relay client lock queue");

/**
 * A client implementation of the relay protocol.
 */
class RelayClient {
    static defaultPort = 62000;

    /**
     * Instantiates a new relay client.
     * @param host
     * @param port
     * @param {Object} [options={}]
     * @param {boolean} [options.skipIdentityCheck=false]
     * @param {Buffer[]} [options.ca]
     */
    constructor(host, port, options = {}) {
        if (typeof options !== 'object')
            throw new TypeError("Expected parameter `options` for Relay.constructor to of type object.");

        this[isLocked] = false;
        this[accessQueue] = [];
        this.socket = null;
        this.users = 0;
        this.socketOptions = {
            port,
            host,
            ca: options.ca,
        };

        this.hasActiveSession = false;

        if (options.skipIdentityCheck === true) {
            console.warn("Warning: skipping identity check - this option should only be used for debugging.");
            this.socketOptions.checkServerIdentity = () => null;
        }
    }

    /**
     * Will lock the client. Locking the client
     * does not put restrictions on sending messages
     * however it does provide a mechanism for waiting
     * until it should be safe to send messages.
     *
     * It is up to the implementer to ensure that
     * they wait until the lock has been freed.
     *
     * Calls to the disconnect function will be ignored
     * while the client is locked.
     * @returns {Promise<singleUseUnlock>}
     */
    lock() {
        const self = this;

        // A unlock functions that can only be used once.
        let locked = true;

        function singleUseUnlock() {
            if (locked) {
                self[isLocked] = locked = false;
                if (self[accessQueue].length > 0) {
                    self[isLocked] = true;
                    self[accessQueue].shift()();
                }
            }
        }

        return new Promise(resolve => {
            if (self[isLocked]) {
                self[accessQueue].push(() => resolve(singleUseUnlock));
            } else {
                self[isLocked] = true;
                resolve(singleUseUnlock)
            }
        });
    }

    /**
     * Attempts to connect to the relay server.
     * If a connection already exists then nothing
     * is done.
     * @returns {Promise}
     */
    connect() {
        if (this.socket != null)
            return Promise.resolve();

        const self = this;
        this.packetParser = new PacketParser();
        return new Promise((resolve => {
            self.socket = tls.connect(self.socketOptions, () => {
                self.socket.pipe(self.packetParser);
                resolve();
            });
        }));
    }

    /**
     * Will disconnect from the remote assuming that the
     * client has not been locked.
     */
    disconnect() {
        if (this.socket == null) {
            console.error("Unable to disconnect from relay, no socket was found.");
            return;
        }

        // If the client is locked we ignore the disconnect call.
        if (this[isLocked]) return;

        this.socket.end();
        this.socket = null
        this.hasActiveSession = false;
    }

    /**
     * Send data to the server.
     * @param {Buffer} data
     */
    send(data) {
        if (this.socket === null)
            throw new Error("A connection must be established before a packet can be sent.");
        this.socket.write(data);
    }

    /**
     * Sends a msg to the given recipient.
     * @param {number} recipient - The intended target recipient of the message.
     * @param {Buffer|String} msg - The message that is to be sent
     * @param {number} [ttl=60] - The lifetime of the message given in minutes.
     */
    async send_to(recipient, msg, ttl = 60) {
        if (!Buffer.isBuffer(msg))
            msg = Buffer.from(msg);

        console.log("Recipient: ", recipient);
        console.log("msg:");
        console.log(msg);

        const payload = Buffer.alloc(6 + msg.length);
        payload.writeUInt32BE(recipient);
        payload.writeUInt16BE(ttl, 4);
        msg.copy(payload, 6);

        console.log("payload");
        console.log(payload)

        const packet = Packet.serialize_parts(packetSend, payload);
        console.log(packet);
        this.send(packet);

        const response = await this.receive_packet();
        if (response.id !== packetResponse)
            throw new ProtocolError(ProtocolError.unexpectedResponseError, 'Unexpected packet type received.');
        switch (response.payload[0]) {
            case responseOk:
                break;
            case responseInvalidPacket:
                throw new ProtocolError(ProtocolError.packetFormatError, response.payload.slice(1).toString());
            case responseError:
                throw new ProtocolError(ProtocolError.relayError, response.payload.slice(1).toString());
            default:
                throw new ProtocolError(ProtocolError.unexpectedResponseError, 'Unexpected response received for initSession request.');
        }
    }


    /**
     * Receive a single packet.
     * @returns {Promise<Packet>}
     */
    receive_packet() {
        if (this.socket === null)
            throw new Error("A connection must be established before a packet can be received.");

        return this.packetParser.nextPacket;
    }

    /**
     * Starts a session.
     * For registering new local-hub devices see {@link create_session}.
     * @param {number} sessionType
     * The type of session being initiated.
     * Either {@link sessionHub} or {@link sessionRemote}.
     *
     * @param {number} deviceId - The id of the device for which the session is to be started.
     * @param {Buffer} identityKey - The private identity key of the device. Used to authenticate the session.
     * @returns {Promise<boolean>} True on success.
     */
    async start_session(sessionType, deviceId, identityKey) {
        if (this.socket === null)
            throw new Error("A connection must be established before a packet can be sent.");
        if (this.hasActiveSession) return Promise.resolve(true);
        if (sessionType === sessionNew)
            throw new Error("Expected sessionType to be either sessionHub or sessionRemote. To create a new session use create_session.");
        if (deviceId < 1)
            throw new RangeError("Expected deviceId to be >= 1");
        if (!(identityKey instanceof Buffer))
            throw new TypeError("Expected identityKey to be of type Buffer");
        let payload = Buffer.alloc(5);
        payload[0] = sessionType;
        payload.writeUInt32BE(deviceId, 1);
        this.send(Packet.serialize_parts(packetSessionInit, payload));

        let response = await this.receive_packet();

        if (response.id === packetResponse) {
            if (response.payload[0] === responseOk) {
                const challenge = response.payload.slice(1);
                const solution = curve25519.ed25519Sign(identityKey, challenge);
                this.send(create_response(responseOk, solution));

                response = await this.receive_packet();
                if (response.payload[0] === responseOk) {
                    this.hasActiveSession = true;
                    return true;
                }
                throw new Error(`Failed authentication. Invalid identity. (${response.payload[0]})`);
            } else if (response.payload[0] === responseInvalidPacket) {
                throw new ProtocolError(ProtocolError.packetFormatError, response.payload.slice(1).toString());
            } else if (response.payload[0] === responseError) {
                throw new ProtocolError(ProtocolError.relayError, response.payload.slice(1).toString());
            } else {
                throw new ProtocolError(ProtocolError.unexpectedResponseError, 'Unexpected response received for initSession request.');
            }
        } else {
            throw new ProtocolError(ProtocolError.unexpectedResponseError, 'Unexpected packet type received.');
        }
    }

    /**
     * Registers a new local-hub with the relay and initiates
     * a session for the device.
     * @param {Buffer} identityKey - The identity key of the local-hub.
     * @param {Buffer[]} remoteKeys - The identity keys for any remotes that are to registered for the hub device.
     * @returns {Promise<*>}
     */
    async create_session(identityKey, remoteKeys = []) {
        if (this.hasActiveSession) return Promise.reject(new Error("Session already active."));
        let payload = Buffer.alloc(4);
        payload[0] = sessionNew;
        payload.writeUInt16BE(identityKey.length, 1);
        payload[3] = remoteKeys.length;
        payload = Buffer.concat([payload, identityKey, ...remoteKeys]);
        this.send(create_packet(packetSessionInit, payload));

        let response = await this.receive_packet();

        const hubId = response.payload.readUInt32BE(1);
        const remotes = [];

        for (let i = 0; i < remoteKeys.length; i++) {
            remotes.push(response.payload.readUInt32BE(5 + i * 4));
        }

        this.hasActiveSession = true;
        return {hubId, remotes};

        // TODO handle errors
    }

    /**
     * Polls the relay for new incoming messages.
     * @returns {Promise<null|*>}
     */
    async poll() {
        this.send(Packet.create_zero_sized(packetPoll));
        let response = await this.receive_packet();
        if (response.id === packetResponse) {

            // Request was successful
            if (response.payload[0] === responseOk) {
                // Nothing was found
                if (response.payloadSize <= 1)
                    return null;

                // Something was found
                try {
                    const messages = RelayClient.deserialize_messages(response.payload.slice(1));
                    this.send(create_response(responseOk));
                    return messages;
                } catch (err) {
                    console.error(err);
                    this.send(create_response(responseFailure));
                }

            }
            // TODO handle errors
        }
        // Something was found but it exceeds the payload limit.
        else if (response.id === packetMultipart) {
            let parts = response.payload.readUInt16BE(headerSize);
            const payloads = [];

            for await (const packet of Packet.create_stream(this.socket)) {
                payloads.push(packet.payload);
                if (--parts === 0)
                    break;
            }

            try {
                const messages = RelayClient.deserialize_messages(Buffer.concat(payloads));
                this.send(create_response(responseOk));
                return messages;
            } catch (err) {
                console.log(err);
                this.send(create_response(responseFailure));
            }
        }

        return null;
    }

    /**
     * Deserializes the contents of a buffer into an array of messages.
     * @param buffer
     * @throws A call to this function may throw if the buffer contents
     *         is not deserializable (when the internal call to {@link Message.from_buffer} throws)
     */
    static deserialize_messages(buffer) {
        const messages = [];
        let retObj; // Storage for the object returned by `from_buffer`
        while ((retObj = Message.from_buffer(buffer)) !== null) {
            messages.push(retObj.message);
            buffer = buffer.slice(retObj.end);
        }
        return messages;
    }

    /**
     * Registers a remote with the relay server.
     * @param {Buffer|number} remoteInfo - Expects a buffer containing an identity key when registering
     * a remote that does not yet have an id. Otherwise the id of the remote is expected.
     * @returns {Promise<null|number>} If a new id was created then it is returned otherwise null will be returned.
     */
    async register_remote(remoteInfo) {
        if (Buffer.isBuffer(remoteInfo)) {
            const payload = Buffer.alloc(remoteInfo.length + 2);
            payload.writeUInt16BE(remoteInfo.length);
            remoteInfo.copy(payload, 2);
            this.send(Packet.serialize_parts(packetAuthRemote, payload));

        } else if (typeof remoteInfo === 'number') {
            const payload = Buffer.alloc(6);
            payload.writeUInt32BE(remoteInfo, 2);
            this.send(Packet.serialize_parts(packetAuthRemote, payload));
        }
        const response = await this.receive_packet();

        if (response.payloadSize === 5) {
            return response.payload.readUInt32BE(1);
        } else {
            return null;
        }
    }
}

module.exports = {
    Packet, PacketParser,
    packetNone, packetMultipart,
    packetResponse, packetSessionInit,
    packetPoll, packetSend,
    packetAuthRemote, maxPayloadSize,
    sessionNew, sessionHub, sessionRemote,
    responseOk, responseFailure, responseInvalidPacket,
    responseError, headerSize, create_packet, create_response,
    ProtocolError, Message, RelayClient
};
