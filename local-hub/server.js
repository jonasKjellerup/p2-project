const https = require('https');
const fs = require('fs/promises');
const dgram = require('dgram');

const defaultAuthPort = 3000;
const defaultUdpPort = 4000;

module.exports.defaultAuthPort = defaultAuthPort;
module.exports.defaultUdpPort = defaultUdpPort;

module.exports.init_auth_server = function (
    authRequestCallback,
    port = defaultAuthPort,
    hostname = '0.0.0.0',
    keyPath = './keys/auth-serv-key.pem',
    certPath = './keys/auth-serv-cert.pem',
) {
    return new Promise(async (resolve, reject) => {
        const options = {};
        try {
            options.key = await fs.readFile(keyPath);
            options.cert = await fs.readFile(certPath);
        } catch (e) {
            reject(e);
            return;
        }

        const server = https.createServer(options, async (req, res) => {
            console.log(`Incoming authentication request from ${req.socket.remoteAddress}:${req.socket.remotePort}.`)
            let body = "";

            req.on('data', chunk => body += chunk);

            await new Promise(resolve => {
                req.on('end', resolve);
            });

            const response = await authRequestCallback(JSON.parse(body));

            res.writeHead(200);
            res.end(JSON.stringify(response));
        });

        server.listen(port, hostname, () => resolve(server));
    });
};

/**
 * Initiates the UDP socket that is responsible for
 * responding to scan inquiries.
 * @param {SignalProtocolAddress} signalAddress
 * @param {number} [port]
 * @returns {Promise<Socket>}
 */
module.exports.init_scan_server = function (signalAddress, port = defaultUdpPort) {
    return new Promise(((resolve, reject) => {
        try {
            const responseMsg = signalAddress.toString();

            // Create UPD socket
            const udpSocket = dgram.createSocket('udp4');

            // Handling incoming messages.
            udpSocket.on('message', (msg, remote) => {
                msg = msg.toString('utf-8');
                if (msg === "Local Hub Search") {
                    console.log(`UDP search packet received from ${remote.address}:${remote.port}`);
                    udpSocket.send(responseMsg, remote.port, remote.address);
                }
            });

            udpSocket.bind(port, () => resolve(udpSocket));
        } catch (e) {
            reject(e);
        }
    }));
};
