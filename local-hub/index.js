const libsignal = require('@p2/libsignal');
const {SessionCipher, SessionBuilder} = libsignal;
const signalCrypto = libsignal.crypto;
const SignalAddress = libsignal.SignalProtocolAddress;
const Store = libsignal.SignalStore;

const protocol = require('../common/relay-protocol.js');
const {RelayClient} = protocol;

const {readFile} = require('fs/promises')
const localServices = require('./server');


const pollRate = 1000;
const relayHost = 'localhost';

/**
 * Polls the relays server.
 * @param {RelayClient} client
 * @param ctx
 * @returns {Promise<void>}
 */
async function poll(client, ctx) {
    const unlock = await client.lock();
    try {
        await client.connect();
        const {deviceId, identity} = ctx;
        await client.start_session(ctx.protocol.sessionHub, deviceId, identity.privKey);
    } catch (e) {
        console.log("Poll failed. Unable to start session.")
        console.error(`Poll failure: ${e}`);
        setTimeout(poll, pollRate, client, ctx);
        return;
    }

    const messages = await client.poll();
    if (messages != null) {
        for (const msg of messages) {
            if (ctx.signalSessions.has(msg.sender)) {
                const session = ctx.signalSessions.get(msg.sender);
                const plaintext = session.cipher.decryptWhisperMessage(msg.contents);
                console.log(`Received message decrypted as: ${await plaintext}`)
            }
        }
    }

    unlock();
    client.disconnect();
    setTimeout(poll, pollRate, client, ctx);
}

/**
 * Initiates the relay session.
 * @param {RelayClient} client
 * @param ctx
 * @returns {Promise<void>}
 */
async function init_relay_session(client, ctx) {
    const unlock = await client.lock();
    await client.connect();
    if (typeof ctx.deviceId === 'number') {
        try {
            await client.start_session(protocol.sessionHub, ctx.deviceId, ctx.identity.privKey);
            console.log('Relay authentication was successful.')
        } catch (e) {
            console.error('Unable to authenticate with relay.');
            console.error(e);
            // TODO delete device id for invalid identity cases.
        }
    } else {
        console.log('No device id found registering anew.');
        ctx.deviceId = (await client.create_session(ctx.identity.pubKey)).hubId;
        console.log(`Registration complete (id = ${ctx.deviceId}).`);
        await ctx.store.setValue('deviceId', ctx.deviceId);
    }

    unlock();
    client.disconnect();
}

async function on_auth_request(data) {
    // Converts deserializes buffer objects

    data.identityKey = Buffer.from(data.identityKey);
    data.preKey.publicKey = Buffer.from(data.preKey.publicKey)
    data.signedPreKey.keyPair.pubKey = Buffer.from(data.signedPreKey.keyPair.pubKey) // TODO the remote should never send the private key
    data.signedPreKey.signature = Buffer.from(data.signedPreKey.signature);

    const response = {};

    const unlock = await this.relayClient.lock();
    await this.relayClient.connect();
    await this.relayClient.start_session(protocol.sessionHub, this.deviceId, this.identity.privKey);
    if (typeof data.address !== 'string') {
        const id = await this.relayClient.register_remote(data.identityKey);
        console.log(`Remote registered with device id ${id}.`)
        if (typeof id !== 'number') {
            console.error('Unknown error encountered while registering remote.');
            return null; // TODO better error reporting
        }

        response.deviceId = id;
        data.address = new SignalAddress(relayHost, response.deviceId);
        console.log("Remote registered");
    } else {
        data.address = SignalAddress.fromString(data.address); // TODO register non new remote
    }

    const remoteAddr = data.address;

    unlock();
    this.relayClient.disconnect();

    const sessionBuilder = new SessionBuilder(this.store, remoteAddr);
    const session = await sessionBuilder.create_session(data);
    const cipher = new SessionCipher(this.store, session);

    response.keyData = {
        identityKey: this.identity.pubKey,
        registrationId: this.deviceId,
        baseKey: session.pendingPreKey.baseKey,
    };

    response.msg = {plaintext: "Hello Client!"};
    response.msg.ciphertext = await cipher.encrypt("Hello Client!")

    this.signalSessions.set(remoteAddr.deviceId, {session, cipher});

    return response
}

(async function init() {
    console.log(`Starting local hub service.\nUsing relay host "${relayHost}".`)
    /*
     If the RELAY_CA environmental variable
     has been set, we load the specified certificate
     and use it as an authority when checking
     for certificate authenticity.
    */
    let ca = undefined;
    if (process.env.RELAY_CA) {
        ca = [await readFile(process.env.RELAY_CA)];
        console.log(`Using CA "${process.env.RELAY_CA} for relay connection."`)
    }

    // Initialises the local database.
    const store = new Store('local-hub-db.sqlite');

    const ctx = {protocol, store, signalSessions: new Map()};


    // Attempts to find a stored identity.
    let identity = await store.identity;

    // Generate new identity if none was found.
    if (identity == null) {
        console.log('No identity found. Generating new identity.');
        identity = signalCrypto.createKeyPair();
        await (store.identity = identity);
    }

    ctx.identity = identity;

    // Attempt to find stored device identifier.
    ctx.deviceId = (await store.getValue('deviceId'))?.value;
    if (typeof ctx.deviceId === 'number') {
        console.log(`Relay device registration id found: ${ctx.deviceId}.`);
        const sessions = store.load_sessions();
        for (const session of sessions) {
            ctx.signalSessions.set(session.registrationId, {
                session,
                cipher: new SessionCipher(store, session),
            });
        }

    }



    const relayClient = new RelayClient(relayHost, RelayClient.defaultPort, {ca, skipIdentityCheck: true});
    ctx.relayClient = relayClient;

    console.log("Initiating relay connection.");
    await init_relay_session(relayClient, ctx);

    ctx.localAddress = new SignalAddress(relayHost, ctx.deviceId);

    console.log("Starting poll loop.");
    setTimeout(poll, pollRate, relayClient, ctx);

    console.log("Starting scan response server.");
    const srServer = await localServices.init_scan_server(ctx.localAddress);

    console.log("Starting auth service server.");
    const authServer = await localServices.init_auth_server(on_auth_request.bind(ctx));


})();
