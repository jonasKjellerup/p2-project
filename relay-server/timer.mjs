/**
 * A module for keeping track of the time-to-live (TTL)
 * of message objects.
 *
 * @module relay-server/timer
 */

import {delete_messages} from "./session.mjs";

// TODO stop timer if the tracked message is retrieved.
// TODO: add checks for negative TTLs (if a message has already expired
//       by the time it is retrieved through `next_ttl`.
// TODO needs unit tests.

/**
 * A class for tracking the TTL of message
 * objects. The object only tracks the time of
 * the message nearest (in time) to its expiration.
 */
class Timer {
    /**
     * Constructs a new Timer instance.
     * @param db - A connected database instance.
     */
    constructor(db) {
        this.db = db;
        this.timeout = null;
    }

    /**
     * The function that is executed once a TTL expires.
     * @returns {Promise<void>}
     */
    async on_timeout() {
        await delete_messages(this.db, [this.target]);
        const next = await this.next_ttl();
        if (next == null) return;
        this.timeout = null;
        this.push_ttl(next.expiration - Date.now(), next._id);
    }

    /**
     * Handles the adding of a new TTL. Since the object
     * only handles a single TTL at a time it is only updated
     * in one of two cases:
     *
     *  1) No timeout is currently running. In this case the timer
     *      is set for the given value.
     *  2) The given message needs to expire before the current tracked
     *      message. In this case we stop the timeout and set it for the
     *      new value.
     * @param {number} ttl - The time remaining til expiration.
     * @param {number} messageId - The id of the message that will be expiring.
     */
    push_ttl(ttl, messageId) {
        if (this.timeout === null) {
            this.target = messageId;
            this.timeout = setTimeout(this.on_timeout.bind(this), ttl);
            this.expirationTime = Date.now() + ttl;
        } else if (this.expirationTime > Date.now() + ttl) {
            clearTimeout(this.timeout);
            this.target = messageId;
            this.timeout = setTimeout(this.on_timeout.bind(this), ttl);
            this.expirationTime = Date.now() + ttl;
        }
    }

    /**
     * Find the message nearest its expiration.
     * @returns {Promise<*>}
     */
    async next_ttl() {
        const [message] = await this.db.collection('messages').find().sort({expiration: 1}).limit(1).toArray();
        return message;
    }
}

export default Timer;
