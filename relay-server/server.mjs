/**
 * @module relay-server/server
 */

import {readFile} from "fs/promises";
import {createServer, Server} from "tls";
import {
    check_packet,
    initiate_counter,
    register_remote,
    create_remote,
    store_message,
    get_hub,
    delete_messages,
    get_incoming_messages,
    init_session
} from './session.mjs';
import * as protocol from '../common/relay-protocol.js';
import Timer from "./timer.mjs";
import config from "../config.mjs";

import mongoDb from 'mongodb';
import {PacketParser} from "../common/relay-protocol.js";

const {MongoClient} = mongoDb;

/**
 * @typedef Session
 * @property {number} id
 * @property {number} deviceType
 */

/**
 * @callback PacketHandler
 * @param {TLSSocket} connection
 * @param {Packet} packet
 * @param {Session} session
 */

/**
 *
 * @type {Object.<number, PacketHandler>}
 */
const packetHandlers = {};

packetHandlers[protocol.packetNone] = function () { /* silently ignores packet */
    return Promise.resolve();
};

packetHandlers[protocol.packetAuthRemote] = async function (connection, packet, session) {
    if (packet.payloadSize < 6) {
        connection.write(protocol.create_response(protocol.responseInvalidPacket, 'Malformed payload, expected at least 6 bytes.'));
        return;
    }
    const keySize = packet.payload.readUInt16BE();
    if (keySize === 0) {
        const remoteId = packet.payload.readUInt32BE(2);
        const registered = await register_remote(this.db, session.id, remoteId);

        if (registered) {
            connection.write(protocol.create_response(protocol.responseOk));
        } else {
            connection.write(protocol.create_response(protocol.responseFailure, 'Remote does not exist.'));
        }
    } else {
        if (packet.payloadSize < 2 + keySize) {
            connection.write(protocol.create_response(protocol.responseInvalidPacket, 'Malformed payload, expected at least ´keySize + 2´ bytes.'));
            return;
        }

        const key = packet.payload.slice(2, keySize + 2);
        const remoteId = await create_remote(this.db, key);
        await register_remote(this.db, session.id, remoteId);
        const response = Buffer.alloc(4);
        response.writeUInt32BE(remoteId);
        connection.write(protocol.create_response(protocol.responseOk, response));
    }
};

packetHandlers[protocol.packetSessionInit] = async function (connection, packet, session) {
    session.id = await init_session.bind(session)(packet, this.db, connection);

    session.deviceType = packet.payload[0] === protocol.sessionNew || packet.payload[0] === protocol.sessionHub ? 1 : 2;

    // if session initialization failed close the connection
    if (session.id === 0)
        connection.end();
};

packetHandlers[protocol.packetSend] = async function (connection, packet, session) {
    if (packet.payloadSize < 6) {
        connection.write(protocol.create_response(protocol.responseInvalidPacket, 'Malformed packet'));
        return;
    }

    const recipient = packet.payload.readUInt32BE();
    const hubId = session.deviceType === 1 ? session.id : recipient;
    const remoteId = session.deviceType === 1 ? recipient : session.id;
    const hub = await get_hub(this.db, hubId);

    if (hub.remotes.includes(remoteId)) {
        const ttl = packet.payload.readUInt16BE(4);
        const message = packet.payload.slice(6);
        const messageId = await store_message(this.db, recipient, session.id, ttl, message);
        this.timer.push_ttl(ttl * 60000, messageId);
        connection.write(protocol.create_response(protocol.responseOk));
    } else {
        connection.write(protocol.create_response(protocol.responseInvalidPacket, 'Sending to the recipient is not authorized.'));
    }
}

packetHandlers[protocol.packetPoll] = async function (connection, _, session) {
    const messages = await get_incoming_messages(this.db, session.id);
    if (messages.length === 0) {
        connection.write(protocol.create_response(protocol.responseOk));
        return;
    }

    let payloadLength = messages.reduce((accumulator, value) => accumulator + 10 + value.message.length, 0);

    if (payloadLength > 65534) {
        const buffer = Buffer.alloc(2);
        buffer.writeUInt16BE(Math.ceil(payloadLength / 65534));
        connection.write(protocol.create_packet(protocol.packetMultipart, buffer));
    }
    const buffer = Buffer.alloc(65534);

    for (const _ of write_messages(buffer, messages)) {
        if (payloadLength > buffer.length) {
            payloadLength -= buffer.length;
            connection.write(protocol.create_response(protocol.responseOk, buffer));
        } else {
            connection.write(protocol.create_response(protocol.responseOk, buffer.slice(0, payloadLength)));
        }
    }

    const packet = await session.parser.nextPacket;
    if (protocol.packetResponse === packet.id) {
        if (packet.payload[0] === protocol.responseOk) {
            delete_messages(this.db, messages.map(message => message._id));
        }
    } else {
        connection.write(protocol.create_response(protocol.responseInvalidPacket, 'Expected response packet.'));
    }
}

function* write_messages(buffer, messages) {
    let cursorPos = 0;

    function write_partial(bytes) {
        const available = buffer.length - cursorPos;
        const slice = bytes.slice(0, available);

        slice.copy(buffer, cursorPos);
        cursorPos += slice.length;

        if (slice.length !== bytes.length) {
            return bytes.slice(slice.length);
        }

        return null;
    }

    for (const message of messages) {
        const integerBytes = Buffer.alloc(4);
        let remainder = integerBytes;

        integerBytes.writeUInt32BE(message.recipient);

        while ((remainder = write_partial(remainder)) !== null) {
            yield;
            cursorPos = 0;
        }

        integerBytes.writeUInt32BE(message.sender);
        remainder = integerBytes;

        while ((remainder = write_partial(remainder)) !== null) {
            yield;
            cursorPos = 0;
        }

        integerBytes.writeUInt16BE(message.message.length);
        remainder = integerBytes.slice(0, 2);

        while ((remainder = write_partial(remainder)) !== null) {
            yield;
            cursorPos = 0;
        }

        remainder = message.message;

        while ((remainder = write_partial(remainder)) !== null) {
            yield;
            cursorPos = 0;
        }
    }
    yield;
}

async function connect_to_db() {
    const client = new MongoClient(config.relayDbUrl, {useUnifiedTopology: true});

    await client.connect();

    const db = client.db('p2-relay');
    await db.command({ping: 1});
    console.log('Connection established to database successfully.');

    const collections = (await db.collections()).map(collection => collection.collectionName);
    const expectedCollections = ["messages", "hubs", "remotes", "counters"];
    const promises = [];

    for (const collection of expectedCollections) {
        if (!collections.includes(collection)) {
            promises.push(db.createCollection(collection));
        }
    }

    await Promise.all(promises);
    return client;
}

export class RelayServer {
    /** @type {MongoClient} */
    db = null;
    /** @type {Server}  */
    listener = null;
    /** @type {Timer} */
    timer = null;

    constructor(config) {
        this.config = config
    }

    async init() {
        try {
            const client = await connect_to_db();
            await initiate_counter(client.db('p2-relay'));
            this.db = client.db('p2-relay');
        } catch (err) {
            throw new Error(`Unable to establish a connection to the database:\n${err}`)
        }

        this.timer = new Timer(this.db);
        const nextTtl = await this.timer.next_ttl();
        if (nextTtl != null) {
            this.timer.push_ttl(nextTtl.expiration - Date.now(), nextTtl._id);
        }
        try {
            const options = {
                key: await readFile(this.config.relayKey),
                cert: await readFile(this.config.relayCert)
            };
            this.listener = createServer(options, this.handle_connection.bind(this));
            this.listener.on('error', err => console.error(err));
        } catch (err) {
            throw new Error(`An error was encountered while initiating TLS server:\n ${err}`);
        }
        return this;
    }

    listen() {
        this.listener.listen(this.config.relayPort, this.config.relayLocalAddress)
    }


    async handle_connection(connection) {
        connection.on('error', err => console.error(err));

        const session = {id: null, parser: new PacketParser()};
        connection.pipe(session.parser);
        for await (const packet of session.parser) {
            // we check if the packet is invalid in the context of the current session
            const invalidity = check_packet(packet, session);
            if (invalidity !== null) {
                connection.write(invalidity);
                continue;
            }

            // if no handler is found we respond with "unknown packet id."
            if (typeof packetHandlers[packet.id] !== 'function') {
                connection.write(protocol.create_response(protocol.responseInvalidPacket, "Unknown packet id."));
                continue;
            }

            try {
                // otherwise invoke handler
                await (packetHandlers[packet.id].bind(this)(connection, packet, session));
            } catch (err) {
                console.error(err);
                const message = Buffer.from('An unexpected error ocurred.');
                connection.write(protocol.create_response(protocol.responseError, message));
            }
        }
    }
}
