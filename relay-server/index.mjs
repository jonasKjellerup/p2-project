import {RelayServer} from "./server.mjs";
import config from "../config.mjs"

(async function init() {
    const server = new RelayServer(config);
    (await server.init()).listen();
})();
