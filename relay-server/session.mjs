import * as protocol from '../common/relay-protocol.js';
import {publicEncrypt, randomBytes} from 'crypto';
import  curve25519 from '@p2/curve25519-native';

export async function create_hub(db, identity, remotes) {
    remotes = remotes.map(async remote => await create_remote(db, remote));
    remotes = await Promise.all(remotes);

    const hub = await db.collection('hubs').insertOne({
        _id: await next_id(db),
        identity,
        remotes,
    })
    return hub.insertedId;
}

export async function create_remote(db, identity) {
    const id = await next_id(db);
    await db.collection('remotes').insertOne({
        _id: id,
        identity,
    })
    return id;
}

export async function initiate_counter(db) {
    const counter = await db.collection('counters').findOne({_id: 'deviceId'});
    if (counter === null) {
        await db.collection('counters').insertOne({_id: 'deviceId', count: 0});
    }
}

export async function next_id(db) {
    const counter = await db.collection('counters').findOneAndUpdate(
        {_id: 'deviceId'},
        {$inc: {count: 1}},
        {returnOriginal: false}
    );
    return counter.value.count;
}

export async function get_hub(db, id) {
    const hub = await db.collection('hubs').findOne({_id: id});
    if (hub != null)
        hub.identity = hub.identity.buffer;
    return hub;
}

export async function get_remote(db, id) {
    const remote = await db.collection('remotes').findOne({_id: id});
    if (remote != null)
        remote.identity = remote.identity.buffer;
    return remote;
}

export async function register_remote(db, hubId, remoteId){
    const remote = db.collection('remotes').findOne({_id: remoteId});

    if(remote === null){
        return false;
    }

    db.collection('hubs').findOneAndUpdate(
        {_id: hubId},
        {$push: {remotes: remoteId}}
    );

    return true;
}

export async function get_incoming_messages(db, recipient) {
    const msgs = await db.collection('messages').find({recipient}).toArray();
    for (const msg of msgs) {
        msg.message = msg.message.buffer;
    }
    return msgs;
}

export async function store_message(db, recipient, sender, ttl, message) {
    const _message = await db.collection('messages').insertOne({recipient, sender, message, expiration: Date.now()+ttl*60000});
    return _message._id;
}

export async function delete_messages(db, messageIds){
    await db.collection('messages').deleteMany({_id: {$in: messageIds}});
}

export async function init_session(packet, db, socket) {
    if(packet.payloadSize < 4){
        socket.write(protocol.create_response(protocol.responseInvalidPacket, 'Malformed packet.'));
        return;
    }

    if (packet.payload[0] === protocol.sessionNew) {
        const keySize = packet.payload.readUInt16BE(1);
        const remoteCount = packet.payload[3];

        if(packet.payloadSize < 4 + keySize * (remoteCount+1)){
            socket.write(protocol.create_response(protocol.responseInvalidPacket, 'Malformed packet.'));
            return;
        }

        const key = packet.payload.slice(4, 4 + keySize);
        const remoteKeys = [];

        for (let i = 0; i < remoteCount; i++) {
            const offset = 4 + keySize + (keySize * i);
            remoteKeys[i] = packet.payload.slice(offset, offset + keySize);
        }

        const hubId = await create_hub(db, key, remoteKeys);
        console.log(`Assigning ${hubId}`);
        const payload = Buffer.alloc(4);
        payload.writeUInt32BE(hubId);
        socket.write(protocol.create_response(protocol.responseOk, payload));
        return hubId;
    } else {
        const id = packet.payload.readUInt32BE(1);
        let device;

        if (packet.payload[0] === protocol.sessionHub) {
            device = await get_hub(db, id);
        } else {
            device = await get_remote(db, id);
        }

        if (device == null)
            return 0;

        const challenge = randomBytes(8);
        const response = protocol.create_response(protocol.responseOk, challenge);
        socket.write(response);
        packet = await this.parser.nextPacket;
        const clientSolution = packet.payload.slice(1);

        if (curve25519.ed25519Verify(device.identity, challenge, clientSolution))
        {
            socket.write(protocol.create_response(protocol.responseOk));
            return id;
        } else {
            socket.write(protocol.create_response(protocol.responseFailure));
            return 0;
        }
    }
}

export function create_challenge(key) {
    return new Promise((resolve, reject) => {
        randomBytes(8, (error, bytes) => {
            if (error) {
                reject(error);
            } else {
                resolve({solution: bytes, challenge: publicEncrypt(key, bytes)});
            }
        })
    });
}

/**
 * Checks the validity of a packet for a given session.
 * @param packet
 * @param session
 * @return {null|Buffer} A buffer is returned when the packet is considered invalid. Otherwise null.
 */
export function check_packet(packet, session) {
    switch (packet.id) {
        case protocol.packetSessionInit:
            if (session.id !== null)
                return protocol.create_response(protocol.responseInvalidPacket, "Session already authorized.");
            break;
        
        case protocol.packetResponse:
            return protocol.create_response(protocol.responseInvalidPacket, "No context for packet response.");
        
        case protocol.packetPoll:
        case protocol.packetSend:
        case protocol.packetAuthRemote:
            if(session.id === null)
                return protocol.create_response(protocol.responseInvalidPacket, "Packet type requires authentication.");
            break;
        
        case protocol.packetMultipart:
            return protocol.create_response(protocol.responseInvalidPacket, "Multipart only valid as response to poll.");
    }

    return null;
}
