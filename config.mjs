export default{
    "relayPort": 62000,
    "relayLocalAddress": "0.0.0.0",
    "relayKey": "./keys/relay-key.pem",
    "relayCert": "./keys/relay-cert.pem",
    "relayDbUrl": "mongodb://hextech.dk:27017/"
}
