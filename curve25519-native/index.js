/* Loads the relevant version of the module for the current platform. */
let version = `${process.platform}-${process.arch}`;
try {
    module.exports = require(`./bin/${version}/curve25519`);
    module.exports.version = version;
} catch (e) {
    console.error(e);
    console.error(`Unable to load native curve25519 module. Your platform (${version}) may not be supported`);
}

/**
 * A Napi wrapper around curve25519 C library.
 * @module curve25519-native
 */

/**
 * Signs the given message with the given private key.
 * @function ed22519Sign
 * @param {Buffer} privKey - A buffer containing a private key exactly 32 bytes in length.
 * @param {Buffer} msg - A buffer containing the msg that is to be signed.
 * @throws The function will throw if given an incorrect argument.
 * @returns {Buffer} Returns a 64 byte signature.
 */

/**
 * Verifies a message signature given a public key.
 * @function ed25519Verify
 * @param {Buffer} pubKey - A buffer containing a public key exactly 33 bytes in length. `pubKey[0]` must be `5`.
 * @param {Buffer} msg - The message that will be tested against the signature.
 * @param {Buffer} signature - The signature that the given msg will be tested against. Must be exactly 64 bytes long.
 * @throws The function will throw if given an incorrect argument.
 * @returns {boolean} Returns false if the msg and signature match. Otherwise returns true. (This is subject to change)
 */

/**
 * Generates a shared key, through an elliptic curve diffie hellman exchange.
 * @function ECDHE
 * @param {Buffer} pubKey - A public key buffer 33 bytes in length.
 * @param {Buffer} privKey - A private key buffer 32 bytes in length.
 * @throws The function will throw if given an incorrect argument.
 * @returns {Buffer} A buffer containing a shared key.
 */

/**
 * Creates a key pair from a given private key.
 * @function createKeyPair
 * @param {Buffer} privKey - A private key buffer 32 bytes in length.
 * @throws The function will throw if given an incorrect argument.
 * @returns {{pubKey: Buffer, privKey: Buffer}} An object containing both the private key and the generated public key.
 */
