// A node.js native wrapper for the ed25519 implementation.

#include <napi.h>
#include <cstdlib>

// Re declares the needed symbols.
// See Gruntfile.js (the compile section) for reference.
extern "C" {

// Originally: typedef crypto_int32 fe[10];
// crypto_int32 is a typedef of int so to simplify
// it becomes:
typedef int fe[10];
typedef struct {
  fe X;
  fe Y;
  fe Z;
  fe T;
} ge_p3;

int curve25519_donna(unsigned char *mypublic, const unsigned char *secret, const unsigned char *basepoint);
void curve25519_sign(unsigned char* signature_out,
                     unsigned char* curve25519_privkey,
                     unsigned char* msg, unsigned long msg_len);
int curve25519_verify(unsigned char* signature,
                      unsigned char* curve25519_pubkey,
                      unsigned char* msg, unsigned long msg_len);
void sph_sha512_init(void *cc);
void crypto_sign_ed25519_ref10_ge_scalarmult_base(ge_p3 *h,const unsigned char *a);

}

namespace Curve25519 {

class Addon : public Napi::Addon<Addon> {
public:
    Addon(Napi::Env env, Napi::Object exports) {
        DefineAddon(exports, {
            InstanceMethod("ed25519Sign", &Addon::Ed25519Sign),
            InstanceMethod("ed25519Verify", &Addon::Ed25519Verify),
            InstanceMethod("ECDHE", &Addon::ECDHE),
            InstanceMethod("createKeyPair", &Addon::CreateKeyPair)
        });
    }

private:
    static void VerifyPrivateKey(Napi::Buffer<unsigned char>& key) {
        Napi::Env env = key.Env();
        if (key.Length() != 32)
            throw Napi::Error::New(env, "expected given private key to be exactly 32 long");
    }

    static unsigned char* VerifyPublicKey(Napi::Buffer<unsigned char>& key) {
        Napi::Env env = key.Env();
        if (key.Length() != 33)
            throw Napi::Error::New(env, "expected given public key to be exactly 33 long");

        unsigned char* data = key.Data();
        if (*data != 5)
            throw Napi::Error::New(env, "expected that pubKey[0] = 5");

        return data + 1;
    }

    Napi::Value CreateKeyPair(const Napi::CallbackInfo& info) {
        Napi::Env env = info.Env();
        if (info.Length() < 1)
            throw Napi::Error::New(env, "createKeyPair(privKey) expects that privKey be a buffer");

        // Gets and checks the given private key
        Napi::Buffer<unsigned char> privKey = info[0].As<Napi::Buffer<unsigned char>>();
        Addon::VerifyPrivateKey(privKey);

        // Copies the private key and transforms it
        Napi::Buffer<unsigned char> privCopy = Napi::Buffer<unsigned char>::Copy(env, privKey.Data(), privKey.Length());
        unsigned char* privData = privCopy.Data();
        *privData &= 248;
        privData[31] = (privData[31] & 127) | 64;

        // Allocates a basepoint
        unsigned char basepoint[32] = {0};
        basepoint[0] = 9;

        // Creates a public key
        Napi::Buffer<unsigned char> pubKey = Napi::Buffer<unsigned char>::New(env, 33);
        unsigned char* pub = pubKey.Data();
        pub[0] = 5;
        curve25519_donna(pub+1, privData, basepoint);

        Napi::Object returnValue = Napi::Object::New(env);
        returnValue.Set("pubKey", pubKey);
        returnValue.Set("privKey", privCopy);

        return returnValue;
    }

    Napi::Value ECDHE(const Napi::CallbackInfo& info) {
        Napi::Env env = info.Env();
        if (info.Length() < 2)
            throw Napi::Error::New(env, "ECDHE(pubKey, privKey) requires that all parameters be buffers");

        Napi::Buffer<unsigned char> _pubKey = info[0].As<Napi::Buffer<unsigned char>>();
        unsigned char* pubKey = Addon::VerifyPublicKey(_pubKey);
        Napi::Buffer<unsigned char> privKey = info[1].As<Napi::Buffer<unsigned char>>();
        Napi::Buffer<unsigned char> sharedKey = Napi::Buffer<unsigned char>::New(env, 32);

        curve25519_donna(sharedKey.Data(), privKey.Data(), pubKey);
        return sharedKey;
    }

    Napi::Value Ed25519Verify(const Napi::CallbackInfo& info) {
        Napi::Env env = info.Env();
        if (info.Length() < 3)
            throw Napi::Error::New(env, "ed25519Verify(pubKey, msg, signature) requires that pubKey, msg, and signature be buffers");
        Napi::Buffer<unsigned char> pubKey = info[0].As<Napi::Buffer<unsigned char>>();
        Napi::Buffer<unsigned char> msg = info[1].As<Napi::Buffer<unsigned char>>();
        Napi::Buffer<unsigned char> signature = info[2].As<Napi::Buffer<unsigned char>>();

        if (signature.Length() != 64)
            throw Napi::Error::New(env, "ed25519Verify(pubKey, msg, signature) expects signature to be exactly 64 long");

        unsigned char* _pubKey = Addon::VerifyPublicKey(pubKey);

        int result = curve25519_verify(signature.Data(), _pubKey, msg.Data(), msg.Length());
        return Napi::Boolean::New(env, result == 0);
    }

    Napi::Value Ed25519Sign(const Napi::CallbackInfo& info) {
        Napi::Env env = info.Env();
        if (info.Length() < 2)
            throw Napi::Error::New(env, "ed25519Sign(privKey, msg) requires that both privKey and msg be buffers");

        Napi::Buffer<unsigned char> privKey = info[0].As<Napi::Buffer<unsigned char>>();
        Napi::Buffer<unsigned char> msg = info[1].As<Napi::Buffer<unsigned char>>();

        Addon::VerifyPrivateKey(privKey);

        Napi::Buffer<unsigned char> signature = Napi::Buffer<unsigned char>::New(env, 64);
        curve25519_sign(
            signature.Data(),
            privKey.Data(),
            msg.Data(),
            msg.Length()
        );

        return signature;
    }
};

NODE_API_ADDON(Addon)

}
