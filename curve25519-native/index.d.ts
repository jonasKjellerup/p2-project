/*
    Documentation file. Exists primarily for better intellisense in webstorm.
    Since curve25519 is a c++/Napi module, intellisense fails to find any exports.
*/
export function ed25519Sign(privKey: Buffer, msg: Buffer): Buffer;
export function ed25519Verify(pubKey: Buffer, msg: Buffer, signature: Buffer): boolean;
export function ECDHE(pubKey: Buffer, privKey: Buffer): Buffer;
export function createKeyPair(privKey: Buffer): {pubKey: Buffer, privKey: Buffer}
