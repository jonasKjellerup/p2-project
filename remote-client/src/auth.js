const https = require('https');
const dgram = require('dgram');
const {SignalProtocolAddress} = require('@p2/libsignal');

const socket = dgram.createSocket('udp4');

// Default port numbers
let remoteAuthPort = 3000;
let remoteScanPort = 4000;
let localScanPort  = 4000;

/**
 * Reads port values from environmental variables.
 * Use RD_REMOTE_AUTH_PORT to set `remoteAuthPort`.
 * Use RD_REMOTE_SCAN_PORT to set `remoteScanPort`.
 * Use RD_LOCAL_SCAN_PORT to set `localScanPort`
 */
function get_env_ports() {
    if (process.env["RD_REMOTE_AUTH_PORT"]) {
        remoteAuthPort = parseInt(process.env["RD_REMOTE_AUTH_PORT"]);
    }

    if (process.env["RD_REMOTE_SCAN_PORT"]) {
        remoteScanPort = parseInt(process.env["RD_REMOTE_SCAN_PORT"]);
    }

    if (process.env["RD_LOCAL_SCAN_PORT"]) {
        localScanPort = parseInt(process.env["RD_LOCAL_SCAN_PORT"]);
    }
}

let options = {
    host: "",
    port: remoteAuthPort,
    path: '/',
    method: 'POST',
    rejectUnauthorized: false,
    header: {
        'Content-Type': 'application/json',
        'Content-Length': 3000,
    },
};
let req = null;
let data = null;

let ipIncr = 1,
    ip,
    broadcasting,
    rotLim = 0;

function broadcast(resolve, reject) {
    const msg = Buffer.from("Local Hub Search");
    socket.send(msg, 0, msg.length, remoteScanPort, `${ip}.${ipIncr}`);
    console.log(`${ip}.${ipIncr++}`);
    if (ipIncr > 255) {
        ipIncr = 1;
        rotLim++;
        if (rotLim >= 5) {
            clearInterval(broadcasting);
            reject(new Error("No local hubs found"));
        }
        
    }
}
socket.on('message', (message, remote) => {
    try {
        const address = SignalProtocolAddress.fromString(message.toString());
        clearInterval(broadcasting);
        options.host = remote.address;
        socket.close();


        console.log(`Hub ${message} found.`);
        socket._resolve({signalAddress: address, ipAddress: remote.address});
    } catch (e) {
        console.log(`Invalid scan response ${message} received.`)
        console.error(e);
    }
});


let LocalConnect = {
    get_env_ports,
    find_local_hub: function() {
        return new Promise(((resolve, reject) => {
            socket._resolve = resolve;
            socket.bind(localScanPort, () => {
                require('dns').lookup(require('os').hostname(), function (err, add, fam) {
                    ip = String(add);
                    ip = ip.slice(0, ip.lastIndexOf('.'));

                    broadcasting = setInterval(broadcast, 1, resolve, reject);
                });
            });
        }));
    },

    connect_to_local_hub: async function(keys) {
        //TCP socket, and exchange prekeys with local hub
        return new Promise ((resolve, reject) => {
            req = https.request(options, (res) => {
                let body = '';
                res.on("data", (chunk) => {
                    body += chunk;
                });

                res.on("end", () => {
                    try {
                        resolve(body);
                    } catch (error) {
                        console.error(error.message);
                    }
                });
            });

            req.write(JSON.stringify(keys));
            req.end();

            req.on('error', error => {
                reject(error);
            });
        });
    },
};

module.exports = LocalConnect;


