const fs = require('fs');
const localConnect = require('./src/auth.js');
const Store = libsignal.SignalStore;
const relayProtocol = require('../common/relay-protocol.js')

const applicationContext = {
    activeSessionInfo: null,
};

let relayOptions = {};

window.addEventListener('DOMContentLoaded', async () => {
    if (process.env.USE_SELF_SIGNED_CA) {
        relayOptions.ca = [fs.readFileSync("../keys/relay-cert.pem")];
    }

    localConnect.get_env_ports();
    init_application_context();

    const sessions = applicationContext.store.load_sessions();
    generate_session_table(sessions);


    document.getElementById('hub-scan').addEventListener('click', on_click_scan);
    document.getElementById('hub-connect').addEventListener('click', on_click_connect);
    document.getElementById('message-send-btn').addEventListener('click', on_click_send);
});

function init_application_context() {
    const store = new Store('remote.sqlite');
    applicationContext.store = store;

    applicationContext.identity = store.identity;
    if (applicationContext.identity == null) {
        applicationContext.identity = signalCrypto.createKeyPair();
        store.identity = applicationContext.identity;
    }
}

function generate_session_table(sessions) {
    const table = document.createElement('table');
    table.classList.add('table')
    const head = document.createElement('thead');
    head.innerHTML =
        `
        <thead>
            <th scope="col">#</th>
            <th scope="col">Remote Id</th>
            <th scope="col">Remote Identity</th>
            <th scope="col">Relay</th>
            <th scope="col"></th>            
        </thead>
        `;
    const body = document.createElement('tbody');

    if (sessions.length === 0) {
        body.innerHTML = "No sessions were found";
    } else {
        for (const session of sessions) {
            console.log(session);
            const row = document.createElement('tr');
            row.innerHTML = `
            <th scope="row">${session.id}</th>
            <td>${session.indexInfo.address.deviceId}</td>
            <td>${session.indexInfo.remoteIdentityKey.toString('hex').substr(0, 8)}</td>
            <td>${session.indexInfo.address.relayHost}</td>
            <td><button class="btn btn-primary" type="button">Select</button></td>
            `;
            row.querySelector('.btn').addEventListener('click', on_click_select.bind(session));
            body.appendChild(row);
        }
    }

    table.append(head, body);
    const container = document.getElementById('sessions-content');
    container.innerHTML = "";
    container.appendChild(table);
}

async function on_click_send() {
    const feedbackText = document.getElementById('outgoing-feedback');
    const msg = document.getElementById('message').value;
    if (msg.length === 0) {
        feedbackText.innerText = "No message was provided.";
        return
    }

    if (applicationContext.activeSessionInfo == null) {
        feedbackText.innerText = "Unable to send msg. No active session found.";
        return;
    }
    feedbackText.innerText = "Sending message... (waiting for relayClient to be free)";
    const {relayClient} = applicationContext.activeSessionInfo;
    const unlock = await relayClient.lock();

    try {
        feedbackText.innerText = "Sending message... (connecting to relay)"
        await relayClient.connect();

        try {
            feedbackText.innerText = "Sending message... (starting relay session)"
            await relayClient.start_session(
                relayProtocol.sessionRemote,
                applicationContext.activeSessionInfo.localId,
                applicationContext.identity.privKey);
        } catch (err) {
            feedbackText.innerText = `Unable to start session ${err}.`
            return;
        }

        const cipherText = (await applicationContext.activeSessionInfo.cipher.encrypt(msg)).body;
        await relayClient.send_to(applicationContext.activeSessionInfo.remoteId, cipherText);


    } catch (err) {
        feedbackText.innerText = `Unable to send message. (err = ${err})`
    } finally {
        unlock();
        relayClient.disconnect();
    }

    feedbackText.innerText = "Message sent";
}

async function on_click_select() {
    console.log(this);
    if (applicationContext.activeSessionInfo) {
        (await applicationContext.activeSessionInfo.relayClient.lock());
        applicationContext.activeSessionInfo.relayClient.disconnect();
    }

    const store = applicationContext.store;

    let remote = this.indexInfo.address;
    let deviceId = store.getValue(`id@${remote.relayHost}`).value;

    applicationContext.activeSessionInfo = {
        localId: deviceId,
        remoteId: remote.deviceId,
        session: this,
        cipher: new SessionCipher(store, this),
        relayClient: new relayProtocol.RelayClient(
            remote.relayHost,
            relayProtocol.RelayClient.defaultPort,
            relayOptions,
        ),
    };

    document.getElementById('message-send-btn').disabled = false;
}

async function on_click_connect() {
    if (!(applicationContext.scanResult?.signalAddress instanceof SignalProtocolAddress))
        return;
    const store = applicationContext.store;

    let session = null;
    let cipher = null
    const hubAddress = applicationContext.scanResult.signalAddress;
    const deviceId = store.getValue(`id@${hubAddress.relayHost}`)?.value;

    let localAddress = null

    if (typeof deviceId === 'number' && deviceId > 0) {
        localAddress = new SignalProtocolAddress(hubAddress.relayHost, deviceId);
        session = store.load_session(hubAddress);
    }

    if (session == null) {
        const {exchangeObject, sessionInfo} = generate_session_info(store, applicationContext.identity, localAddress);
        const hubInfo = JSON.parse(await localConnect.connect_to_local_hub(exchangeObject));
        parse_data(hubInfo);

        if (localAddress == null) {
            localAddress = new SignalProtocolAddress(hubAddress.relayHost, hubInfo.deviceId);
            store.setValue(`id@${localAddress.relayHost}`, localAddress.deviceId);
        }

        sessionInfo.baseKey = hubInfo.keyData.baseKey;
        sessionInfo.identityKey = hubInfo.keyData.identityKey;
        const sessionBuilder = new SessionBuilder(store, hubAddress);
        session = sessionBuilder.create_session_from_prekeys(sessionInfo);

        cipher = new SessionCipher(store, session);
        const plaintext = await cipher.decryptWhisperMessage(hubInfo.msg.ciphertext.body);
        if (plaintext.toString() !== hubInfo.msg.plaintext)
            throw new Error(`Decrypt failed ${plaintext} !== ${hubInfo.msg.plaintext}`);
    } else {
        cipher = new SessionCipher(store, session);
    }


    applicationContext.activeSessionInfo = {
        localId: localAddress.deviceId,
        remoteId: hubAddress.deviceId,
        session,
        cipher,
        relayClient: new relayProtocol.RelayClient(
            hubAddress.relayHost,
            relayProtocol.RelayClient.defaultPort,
            relayOptions
        ),
    };

    document.getElementById('message-send-btn').disabled = false;

    // Updates the session table.
    const sessions = applicationContext.store.load_sessions();
    generate_session_table(sessions);
}

async function on_click_scan() {
    const sectionContent = document.getElementById('local-hub-content');
    sectionContent.innerHTML = "Scanning for hubs...";

    try {
        const hub = await localConnect.find_local_hub();
        applicationContext.scanResult = hub;
        sectionContent.innerHTML =
            `<b>Local hub found:</b><br/>
             IP-Address: ${hub.ipAddress}<br/>
             Signal address: ${hub.signalAddress}`;
        document.getElementById('hub-connect').disabled = false;
    } catch (err) {
        sectionContent.innerHTML = `Scan failed: ${err}`;
    }
}
