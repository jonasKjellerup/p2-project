const bootstrap = require('bootstrap');
const libsignal = require('@p2/libsignal');

const signalCrypto = libsignal.crypto;
const {KeyHelper, SignalProtocolAddress, SessionBuilder, SessionCipher} = libsignal;

function generate_session_info(store, identity, address = null) {
    const preKey = signalCrypto.createKeyPair();
    const keyId = 1;

    const identityKey = identity.pubKey;
    const signedPreKey = KeyHelper.generateSignedPreKey(identity, keyId);

    return {
        exchangeObject: {
            address: address?.toString(),
            identityKey,
            preKey: {
                keyId,
                publicKey: preKey.pubKey,
            },
            signedPreKey,
        },
        sessionInfo: {
            preKey,
            signedPreKey: signedPreKey.keyPair,
        }
    };
}

function parse_data(data) {
    data.keyData.baseKey = Buffer.from(data.keyData.baseKey.data);
    data.keyData.identityKey = Buffer.from(data.keyData.identityKey.data);
    data.msg.ciphertext.body = Buffer.from(data.msg.ciphertext.body);
}



